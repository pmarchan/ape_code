#!/bin/bash

SECONDS=0

echo ""
echo "Starting APE test-suite"
echo "This should only take a minute"
echo ""

test_failed=0
NC='\033[0;0m'
GREEN='\033[0;32m'
RED='\033[0;31m'


for dir in test_snapshot/ test_grains/ test_particle/ test_gridofpart/
do
  cd $dir

  echo "Running test $dir..."

  # Find type of run, snapshot or particle
  line=$(grep "particle=" parameters.nml)
  snaporpart=${line:10:5}
  if [ $snaporpart == "false" ]
  then
    line2=$(grep "radmc_output=" parameters.nml)
    sphorrad=${line2:14:5}
    if [ $sphorrad == "false" ]
    then
      outfile="output_ape_sph.dat"
    else
      outfile="dust_density.inp"
    fi
  else
    line=$(grep "grid_of_part=" parameters.nml)
    isitgridofpart=${line:14:4}
    if [ $isitgridofpart == "true" ]
    then
      outfile="particles/part_00025/structure_evolution.dat"
    else
      outfile="part_traj.dat"
    fi
  fi

  # Do the run
  ../../src/ape parameters.nml > log
  
  # Difference with reference result
  diff $outfile reference_result.dat > difference.dat

  passed=0
  nlinesdiff=$(wc -l difference.dat  |  awk '{print $1}')
  if [ -e $outfile ] && [ $nlinesdiff -eq 0 ]
  then
    passed=1
  fi


  # Display result
  if [ $passed -eq 0 ]
  then
    test_failed=$((test_failed+1))
    printf "${RED}TEST FAILED${NC}\n"
    echo ""
  else
    printf "${GREEN}TEST SUCCESS${NC}\n"
    echo ""
    rm -rf $outfile difference.dat log *inp structure_evolution.dat output_ape_sph.dat particles/
  fi

  cd ..

done


# Test-suite result
if [ $test_failed -eq 0 ]
then
  printf "${GREEN}Congratulations, you did not break anything${NC}"
else
  if [ $test_failed -eq 1 ]
  then
    printf "${RED}There was $test_failed failed test, better check it out !${NC}"
  else
    printf "${RED}There were $test_failed failed tests, better check them out !${NC}"
  fi
fi
echo " "


echo " "
echo "Test-suite run in $SECONDS seconds"
echo ""
