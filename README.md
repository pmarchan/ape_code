# APE


Welcome to the APE code : Analytical Protostellar Environment.
Credit : Pierre Marchand
pierre.marchand.astr@gmail.com
Financial support : European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (ERC Starting Grant “Chemtrip”, grant agreement No 949278)

The main purpose of APE is to provide physical conditions to astrochemical and radiative transfer codes in order to make synthetic observations.
APE generates a 2D axi-symmetric map of a collapsing envelope, protoplanetary disk, protostar and outflow. The output files can then directly be used as input for Nautilus (for the astrochemistry), RADMC-3D (radiative transfer) and Imager (synthetic imaging). See the "Advanced Uses" section for more details.
For the detailed physical models, please refer to the "Ape_methods.pdf" file.


## Quick start

Go in the src/ directory and compile with  
$ make  
! You may need to switch the compiler from "mpif90" to "gfortran" if you don't have it installed.
! If the path to APE contains spaces, parenthesis, or other special characters, the compilation may fail.
You can test your installation by running the test-suite :
$ cd ..
$ cd test_suite
$ ./run_test_suite.sh

Go back in the parent directory and you can now use APE however you like.
Start by choosing your parameters value in parameters.nml. For simplicity, only change "MODEL_PARAMS" parameters at first. See the "Parameter list details" section for an explanation of each parameter, and the Ape_methods.pdf file for the mathematical model behind.
You can then run the code by typing
$ src/ape parameters.nml  
There are two main modes : snapshot and particle.

### Snapshot mode (particle = .false.)
This mode generates a snapshot of the system at time t on a grid of your choosing.   
The data is dumped in the file "output_ape_sph.dat" with all the variables of the grid.
To plot the data, run > python3 scripts/plot_density_map.py. You can choose the range of the plot by modifying rmaxplot in the python script.  


### Particle mode (particle = .true.)
This mode simulates the trajectory of a particle through its journey during the collapse, starting from an initial x_ini,z_ini position. There is no grid in this mode, only the local information is calculated (+ some disk structure). The run stops when it reaches the maximum time defined in the parameter file, or if the particle falls into the protostar or the outflow. Setting reverse=.true. in the parameter file makes the time move backward : x_ini and z_ini now designate the final position of the particle at time t, and the trajectory is calculated for time t to time 0.


### Examples
Few examples are provided in the examples/ directory. To run them, go in the desired directory and type  
$ ./run.sh
Then plot the results by executing the python script in the directory.

### Test-suite
A test-suite is provided to make sure you did not break anything if you decide to make modifications to the code. To run it, go to the test_suite directory and run
./run_test_suite.sh


### Miscellaneous
To avoid numerical issues, the density decreases exponentially in the inner 2 au, be careful when considering physical properties in this region.
Uncomment line 18 of the Makefile to compile with debugging options.


## Parameter list details :

### GRID_PARAMS
- loggrid = .true. or .false.. If .true., the grid will have a log spacing in radial coordinates. The inner cell size will be set to resol. nrad needs to be defined. 
- rad_min_au = innermost cell in au.
- rad_max_au = size of the calculation box in au.
- nrad = Number of points in the radial direction
- theta_min_deg = Mimimum polar angle of the window (theta_min=0 is the vertical axis along the outflow).
- theta_max_deg = Maximum polar angle of the window (theta_max=90 is the mid-plane f the disk)
- ntheta = resolution in angle theta from 0 to pi/2.
- radmc_output = .true. or .false.. If .true., the code will output the .inp files needed to run RADMC3d.
- snapshot_nautilus = .true. or .false.. If .true., the code will generate static particles for all the cells in the grid, in the directory particles/. This is effectively a way to run Nautilus on a static snapshot.
- nautilus_time_kyr = Simulation time desired in kyr when running Nautilus on the static snapshot.

### MODEL_PARAMS
- Mass = Mass of the initial cloud in Msun.
- time_years = Most important parameter to set : time in years after the cloud started collapsing. A protostar can be formed only after a free-fall time.
- t_pstar_age = .true. or .false. If .true., time_years is the age of the protostar. If .false., it is the time since the beginning of the collapse.
- temp_mol_cloud = Temperature of the surrounding molecular cloud (miminum envelope temperature).
- masstoflux = Initial mass-to-flux ration of the cloud. Affects the magnetic field, hence the size of the disk. A higher mass-to-flux ratio yields a weaker magnetic field and a larger disk.
- coagulation = Enable the coagulation of grains (read in tablecoag_20bin.dat). If false, just use the initial MRN of the table.
- dust_to_gas = Desired dust-to-gas ratio. !!!! With the value 1d-2, the coagulation model of Lebreuilly+23 is used. For any other value, Marchand+23 is used !!!!
- alphadisk = Value of the disk alpha parameter. Essentially controls the temperature and scale-height of the disk.
- disk_cutoff = If true, sharply cut the disk at r=rdisk. If false (default) let the disk continue until blended in the envelope.
- use_radmc_temp = If .true., use interpolation from radmc results to get the temperature in particle or snapshot mode. WARNING : necessitate appropriate radmc3d calculation beforehand.

### PARTICLES
- particle = .true. or .false., enable or disable particle mode.
- tmax = Maximum time in years at which the integration stops.
- dtini = Maximum allowed time-step in years. This is in addition to some kind of adaptive time-stepping.
- dyn_fact = Limit the time-step so that dt <= 1/dyn_fact * r/v_r. Higher values of dyn_fact result in smaller time-steps.
- x_ini, z_ini = initial position of the particle in au.
- omega0 = Initial angular rotation velocity of the cloud in s-1 (Typically 1e-13, generally between 1e-15 and 1e-12).
- cloud_extinction = Extinction coming from the surrounding molecular cloud (Av).
- reverse = if set to .true., the trajectory will be integrated backward in time, starting at the time defined in time_years, ending at t=0.
- radmc_dir = directory path to the radmc temperature dir temp_radmc/
- steadystate = If you want a steady-state system (star, disk, outflow...) for your particles. In that case, uses a snapshot at the time defined in time_years.
- grid_of_part = set to .true. if you want to make a map with particles to follow their evolution (forward or backward). Will use the parameters of the grid.

### OVERRIDE_PARAMS
- override = .true. or .false.. Override the algorithm to impose disk and/or star properties. Leave any of the following values at 0d0 to not override the algorithm for that property.
- mdisk_over in Msol
- rdisk_over in au
- rdisk_in_over in au
- mstar_over in Msol
- rstar_over in Rsol
- Tstar_over in K
- Lstar_over in Lsol
- outflow_angle_over in degrees (<90)



## Advanced use

### Requirements for more advanced uses
APE has been designed to run chemical calculations and perform synthetic observations in conjunction with other publicly available softwares. You are free to use whichever software you like, but we only provide scripts for specific codes. We recommend to install the following softwares if you do not have them already.
- For running scripts : **Python 3**
- Radiative transfer : **RADMC-3D** (https://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/index.php)
- Chemistry : **Nautilus** (https://forge.oasu.u-bordeaux.fr/LAB/astrochem-tools/pnautilus)
- Synthetic imaging : **Imager** (https://imager.oasu.u-bordeaux.fr/)


### How to use the radmc temperature for a snapshot ? (Optional)
1. Choose your model parameters in parameters.nml. Set use_radmc_temp=.false. and radmc_output=.true.
2. Run APE.
3. Run "radmc3d mctherm -setthtreads N", with N the number of OpenMP threads. !!! Longest step (up to a few cpu.h)!!!
4. Run "python3 scripts/average_temp_radmc3d.py"
5. Change use_radmc_temp=.true. in parameters.nml
6. Run APE again.
7. The resulting snapshot uses the temperature map calculated by radiative transfer.


### How to use the radmc temperature for particles ? (Optional)
1. Choose your model parameters in parameters.nml. Set particle=.false. and radmc_output=.true..
2. Choose your radmc snapshots parameters at the begining of the scripts/setup_radmc3d.sh : name of the model, time sampling parameters.
3. Run scripts/setup_radmc3d.sh. That will : a. create a directory simu_radmc/model_name/ and several sub-directories for all the radmc runs, b. run APE for all the required snapshots (determined by the time sampling), c. copy all the .inp files in the sub-directories.
4. Go to the simu_radmc3d/setup_name directory, and run ../../scripts/do_all_radmc_snapshots.sh (choose your number of cpus in the script) !!! Longest step (~few cpu.h per snapshot)!!!
5. Run '../../scripts/gather_average_temperature.sh' to average the temperature of the size distributions, which will copy the results in simu_radmc/model_name/temp_radmc/
6. You can now go back to the ape_code/ directory, change parameters.nml with particle=.true., use_radmc_temp=.true and radmc_dir="simu_radmc/model_name/". Works for single particles and grid of particles.


### How to make chemical abundances maps ?
1. Run APE in snapshot mode to check if you are satisfied with the physical setup (protostar age, disk size, density and temperature maps ...).
2. (Optional) Compute temperature maps with RADMC-3D (see previous section).
3. Run APE again with the same parameters, changing to particle=.true., grid_of_part=.true. and reverse=.true..
   This might be a long step (~50-100 cpu.h), you may want to
   a. Change the compiler to F90=mpif90 in the Makefile,
   b. Run > make clean; make,
   c. Run APE using > mpif90 -np N ./ape parameters.nml; with N the number of cpus.
3. ALTERNATIVE : You can also run nautilus on a static snapshot instead of the grid_of_particles mode. In parameters.nml, you need to set particle=.false., snapshot_nautilus_output=.true. and a simulation time with snapshot_nautilus_output_time_kyr. The other steps are the same.
4. Copy the Nautilus executable in the "Nautilus_files" directory. Example Nautilus parameter files are already present (taken from the public version), you may wish to change them to whatever you need for your chemical network.
5. Choose your number of CPUs in ./scripts/do_all_nautilus.sh and run the script from the APE home directory (or whichever directory is the parent of the particles/ directory). That will run nautilus in each particles/part_XXXXX directory. !!! Long step (at least several minutes.cpu per particle)!!!
6. Run 'python3 scripts/generate_numberdens_file.py MMM' with MMM the molecule you want. This will create the files abundance_MMM.inp and numberdens_MMM.inp. Running "scripts/plot_abundance_map.py MMM" will plot the abundance map from the abundance_MMM.inp file, and the numberdens_MMM.inp file is used for synthetic observations.
7. Repeat step 6. for all other species of interest.


### How to make synthetic observations ?
1. Make a chemical abundance map of molecules you wish to study (see previous section).
2. Edit the make_synthetic_observation.py file to input the parameters you want. Basically, you input a frequency range in GHz (**FreqMinGHz** - **FreqMaxGHz**) and a chemical species (**species**). This script will look for emission lines of this species in this frequency range, and compute the data cube of the map around the frequencies of the transitions. You can customize the frequency resolution in GHz (**FreqResolGHz**), the spectral width of the cube around each line in km/s (**DeltaKms**), the **Inclination**, the **vlsr** of the source, the distance of the source in pc (**dist__pc**), the noise, the coordinates, and the ALMA configuration and observing time. REQUIREMENT : you need a file with the spectroscopic information on the chemical species in the "linelist" format (see the provided CO file lib/linelist_CO.inp, and the RADMC-3D manual). This step creates FITS files of emission cubes and a ready-to-use script for Imager.
3. Go to the created directory (e.g. : CO_115.270GHz_incl0/), open Imager and run @your_script_name.ima.
4. You now have simulated observations under the name "clean_synthetic_Image_XXXXX.fits" and "baseline_synthetic_Image_XXXXX.fits" (one per spectral window).


### Additional documentation about the make_synthetic_observation.jl script
- You need a file with the spectroscopic information about the species you choose. By default, the script uses the "linelist" format for RADMC-3D. If you wish to use another format (see the RADMC-3D documentation), there may be heavy adjustments to make to the script.
- You can choose your ALMA configuration with the parameter "ALMAconfig". It can be one configuration, or a combination of two configurations. The possible configurations are : [1],[2],[3],[4],[5],[6],[7],[8],[9],[4,1],[5,2],[6,3],[7,4],[8,5],[9,6]. The maximum recovery scale and beam size are listed in the ALMA documentation. When combining two configurations, the second configuration observing time is automatically scaled to match the observation time ratios recommended by ALMA.
- The noise is set by hand with the parameter "NoiseAve". The observing time only affects the uv coverage. If you need reliable noise levels, you may need to use the AOT or the ALMA documentation and simulator.
- The pixel size in the synthetic cubes is set by Imager and depends on the ALMA configuration. In the script, you can choose the number of pixels per beam size for the emission maps generated by RADMC-3D (= the input of Imager) with the parameter FactPixPerBeam. For example, if your beam is 1.4'' (determined by the ALMA configuration), and you choose FactPixPerBeam=16, then each pixel in the emission map will have a size of 1.4/16 = 0.0875''.
