#!/bin/bash

# Calculate the average temperature with a weighted average of the grain temperature (on the surface)

mkdir temp_radmc/

ii=0
for dirrun in run*/
do
  cd $dirrun
  echo $dirrun
  cp ../../../parameters.nml .
  # Extract grid parameters
  touch grid_par.dat
  res=$(grep "resol=" parameters.nml)
  res2=${res:6}
  echo $res2 >> grid_par.dat
  res=$(grep "lbox=" parameters.nml)
  res2=${res:5}
  echo $res2 >> grid_par.dat
  res=$(grep "nlog=" parameters.nml)
  res2=${res:5}
  echo $res2 >> grid_par.dat
  ii=$(echo "$ii+1" | bc -l)
  # Average temperature
  python3 ../../../scripts/average_temp_radmc3d.py
  cp gas_temperature.dat ../temp_radmc/gas_temperature_$ii.dat
  rm grid_par.dat
  cd ..
done
