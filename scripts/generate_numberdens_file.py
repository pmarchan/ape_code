import numpy as np
import sys


mump=1.67e-24*2.31

species=sys.argv[1:][0]

# Read data
f=open("output_ape_sph.dat","r")
data=f.read().split("\n")
data.pop(-1) # The last line is empty

# Read parameters
loggrid=int(data[0].split(" -- ")[0])
resol=float(data[1].split(" -- ")[0])
lbox=float(data[2].split(" -- ")[0])
nr=int(data[3].split(" -- ")[0])
ntheta=int(data[4].split(" -- ")[0])
nvar=12
f.close()

# Read density data
filedens_gas="gas_density.inp"
fdens_gas=open(filedens_gas,"r")
datadens_gas=fdens_gas.read().split("\n")
fdens_gas.close()

filename="numberdens_"+species+".inp"
filename2="abundance_"+species+".inp"
f=open(filename,"w")
f2=open(filename2,"w")
f.write("1\n")
f2.write("1\n")
npart=nr*ntheta
f.write(str(npart)+"\n")
f2.write(str(npart)+"\n")

ipart2=-1

for j in range(ntheta):
  for i in range(nr):
    ipart2+=1
    ipart=1+j+i*ntheta
    ichar=("%05d" % ipart)
    file1="particles/part_"+ichar+"/abundances.tmp"
    with open(file1) as fp:
      for line in fp:
        if line.startswith(species):
          ab=float(line.split("=")[1])
          dens=float(datadens_gas[ipart2])
          numdens=ab*dens/mump*2.0
          f.write(str(numdens)+"\n")
          f2.write(str(ab)+"\n")
          break
f.close()
f2.close()
