# Plot temperature map from radmc-3d output

import matplotlib.pyplot as plt
import numpy as np

# To chose
rmaxplot_au=70

val_min=10.0
val_max=1000.0

# Read data
f=open("output_ape_sph.dat","r")
params=f.read().split("\n")
f.close()

# Read parameters
loggrid=int(params[0].split(" -- ")[0])
rad_min=float(params[1].split(" -- ")[0])
rad_max=float(params[2].split(" -- ")[0])
nrad=int(params[3].split(" -- ")[0])
theta_min=float(params[4].split(" -- ")[0])
theta_max=float(params[5].split(" -- ")[0])
ntheta=int(params[6].split(" -- ")[0])
nvar=13

f=open("gas_temperature.dat","r")
data=f.read().split("\n")
data.pop(-1)
f.close()

# Build grid
theta=np.linspace(np.pi/2.0-theta_min,np.pi/2.0-theta_max,ntheta)
if loggrid:
  r=np.logspace(np.log10(rad_min),np.log10(rad_max),nrad)
else:
 r=np.linspace(rad_min,rad_max,nrad)
R,THETA=np.meshgrid(r,theta)
ZZ=np.zeros((ntheta,nrad))
sphere=np.zeros(nrad*ntheta)

# Put data in array
for i,dat in enumerate(data):
  sphere[i]=float(dat)

# Read density
for i in range(ntheta):
  ZZ[:][i]=sphere[i*nrad:(i+1)*nrad]

# Plot things
plt.figure(figsize=(11,6))
ax1=plt.subplot(121,projection='polar')
ax1.set_thetamin(0)
ax1.set_thetamax(90)
ax1.grid(False)
im=ax1.pcolormesh(THETA,R,ZZ,cmap='hot',vmin=val_min,vmax=val_max,shading="auto")
plt.axis([np.pi/2.0-theta_max,np.pi/2.0-theta_min,0,rmaxplot_au])
cbar=plt.colorbar(im,ax=ax1,fraction=0.046,pad=0.06)
cbar.set_label("Temperature (K)",rotation=270,labelpad=20)
plt.gca().set_rmin(0.0)
ax2=plt.subplot(122,projection='polar')
nlvl=10
levels=np.linspace(val_min,val_max,nlvl)
cp=ax2.contourf(THETA,R,ZZ,levels=levels,cmap="hot",vmin=val_min,vmax=val_max)
ax2.grid(False)
plt.axis([np.pi/2.0-theta_max,np.pi/2.0-theta_min,0,rmaxplot_au])
cbar=plt.colorbar(cp,ax=ax2,fraction=0.046,pad=0.06)
cbar.set_label("Temperature (K)",rotation=270,labelpad=20)
plt.gca().set_rmin(0.0)

plt.savefig("Map_radmc3d_temperature.png")
plt.show()
