import numpy as np
import os

# Weighted average of the temperature from radmc output

pi=np.pi
rhodust=2.3

f=open("dust_temperature.dat","r")
data=f.read().split("\n")
data.pop(0)
ncells=int(data.pop(0))
nbins=int(data.pop(0))
data.pop(-1)
data_temp=[float(x) for x in data]
f.close()
dust_temp=np.zeros((nbins,ncells))
for i in range(nbins):
  dust_temp[i,:]=data_temp[i*ncells:(i+1)*ncells]

f=open("dust_density.inp","r")
data=f.read().split("\n")
data.pop(0)
data.pop(0)
data.pop(0)
data.pop(-1)
data_dens=[float(x) for x in data]
f.close()
dust_dens=np.zeros((nbins,ncells))
for i in range(nbins):
  dust_dens[i,:]=data_dens[i*ncells:(i+1)*ncells]

f=open("dust_sizes.inp","r")
data=f.read().split("\n")
data.pop(-1)
data_sizes=[float(x) for x in data]
f.close()
dust_mass=[4.0/3.0*pi*s**3.0*rhodust for s in data_sizes]


if os.path.isfile("output_ape_sph.dat"):
  f=open("output_ape_sph.dat","r")
else:
  f=open("../output_ape_sph.dat","r")
data_params=f.read().split("\n")
f.close()
resol=float(data_params[1].split(" -- ")[0])
lbox=float(data_params[2].split(" -- ")[0])
nr=int(data_params[3].split(" -- ")[0])

gas_temp=np.zeros(ncells)
f1=open("gas_temperature.dat","w")
for j in range(ncells):
  temp=0.0
  weight=0.0
  for i in range(nbins):
    ndust=dust_dens[i,j]/dust_mass[i]
    temp+=dust_temp[i,j]*ndust*data_sizes[i]**2.0
    weight+=ndust*data_sizes[i]**2.0
  gas_temp[j]=temp/weight
  f1.write('%15.8e\n' % gas_temp[j])

f1.close()

#with open("scripts/plot_temp_radmc3d.py") as f:
    #exec(f.read())



