# This routine projects column density according to an inclination

# Parameters
const species="HCN" # file numberdens_XXX.inp required (XXX=species)
const angle=0.0 # Between 0 and 90
const npix=300 # Resolution across the box
const crop=100 # In au, to reduce the size of the window (zoom)

##
const angle_radian=angle*pi/180.0
const cosA=cos(angle_radian)
const sinA=sin(angle_radian)

## Constants
const au=1.5e13

# Loading data
file=open("amr_grid.inp","r")
data_amr=readlines(file)
close(file)
file=open("numberdens_$species.inp","r")
data_numberdens=readlines(file)
close(file)


# Rearranging data

## AMR
nrntheta=split(data_amr[6],"    ")
const nr=parse(Int64,nrntheta[1])
const ntheta=parse(Int64,nrntheta[2])
const r_array=parse.(Float64,split(data_amr[7],"    "))./au
const theta_array=parse.(Float64,split(data_amr[8],"    "))
const rmax=r_array[end]
const rmin=r_array[2]
delta_i_crop=round(Int64,crop/rmax*npix/2.0)
const iminplot=round(Int64,npix/2-delta_i_crop)
const imaxplot=round(Int64,npix/2+delta_i_crop)

## Numberdens
numberdens=Array{Float64,2}(undef,nr,ntheta)
for j=1:ntheta
  for i=1:nr
    numberdens[i,j]=parse(Float64,data_numberdens[2+i+(j-1)*nr])
  end
end


# Functions

## Simple roots of ax^2+bx+c=0
function roots(a,b,c)
  delta=b^2-4*a*c
  if delta < 0
    return (1e99,1e99)
  end
  root1=(-b-sqrt(delta))/(2*a)
  root2=(-b+sqrt(delta))/(2*a)
  return (root1,root2)
end

## L2 norm
function norml2(arr)
  return sqrt(sum(arr.^2.0))
end

## xyz coordinates along the ray, parametric function
function get_xyz_from_xij_t(xij,yij,zij,t)
  return (xij-t*sinA,yij,zij-t*cosA)
end

## Get pixel position in space as a function of local grid coordinates (base change)
function get_xij_from_XY(Xij,Yij,R)
  return (R*sinA-Yij*cosA, Xij, R*cosA+Yij*sinA)
end

## Get radial coordinate depending on t
function get_r_from_t(xij,yij,zij,t)
  (x,y,z)=get_xyz_from_xij_t(xij,yij,zij,t)
  return norml2([x,y,z])
end

## Get theta angle depending on t
function get_theta_from_t(xij,yij,zij,t)
  (x,y,z)=get_xyz_from_xij_t(xij,yij,zij,t)
  return acos(abs(z)/norml2([x,y,z]))
end

## Recover which t parameter corresponds to r
function get_t_from_r(xij,yij,zij,r)
  a=1.0
  b=-2.0*(xij*sinA+zij*cosA)
  c=norml2([xij,yij,zij])^2.0-r^2.0
  return roots(a,b,c)
end

## Recover which t parameter corresponds to theta
function get_t_from_theta(xij,yij,zij,theta)
  a=cos(theta)^2.0-cosA^2.0
  b=2*(-cos(theta)^2.0*(xij*sinA+zij*cosA)+zij*cosA)
  c=norml2([xij,yij,zij])^2.0*cos(theta)^2.0-zij^2.0
  return roots(a,b,c)
end

## Get index from coord
function get_i_from_rtheta(rt,rt_array)
  if rt<rt_array[2]
    return 1
  end
  if rt>rt_array[end-1]
    return nr
  end
  imin=1
  imax=nr
  imid=round(Int64,nr/2)
  while imax-imin > 1
    rtmid=rt_array[imid]
    if rt > rtmid
      imin=imid
    else
      imax=imid
    end
    imid=round(Int64,(imin+imax)/2)
  end
  return imax
end


# Image coordinates
function get_X_from_i(R,i,n)
  return R*((2.0*i-1.0)/n-1.0)
end


# Build image grid
Image=Array{Float64,2}(undef,npix,npix)
Image.=0.0


# Main ray-tracing function
function ray_trace_integrate(xij,yij,zij,numberdens)
  tbounds=get_t_from_r(xij,yij,zij,rmax)
  tstart=minimum(tbounds)
  tend=maximum(tbounds)
  tcur=tstart

  theta_start=get_theta_from_t(xij,yij,zij,tstart)
  ir=nr
  itheta=get_i_from_rtheta(theta_start,theta_array)
  (xcur,ycur,zcur)=get_xyz_from_xij_t(xij,yij,zij,tcur)

  total=0.0

  while tcur < tend

    # Parameter t for next r boundary
    ir_plus=min(ir+1,nr)
    ir_minus=max(ir-1,1)
    r_next_1=r_array[ir_plus]
    r_next_2=r_array[ir_minus]
    r_next_3=r_array[ir]
    t_r_1=get_t_from_r(xij,yij,zij,r_next_1)
    t_r_2=get_t_from_r(xij,yij,zij,r_next_2)
    t_r_3=get_t_from_r(xij,yij,zij,r_next_3)
    t_r_all=[t_r_1[1],t_r_1[2],t_r_2[1],t_r_2[2],t_r_3[1],t_r_3[2]]
    ir_all=[ir_plus,ir_minus,ir]
    t_r_next=1e99
    iind_r=0
    for (i,t_r) in enumerate(t_r_all)
      if t_r <= t_r_next && t_r > tcur
        t_r_next=t_r
        iind_r=i
      end
    end

    # Parameter t for next theta boundary
    itheta_plus=min(itheta+1,ntheta)
    itheta_minus=max(itheta-1,1)
    theta_next_1=theta_array[itheta_plus]
    theta_next_2=theta_array[itheta_minus]
    theta_next_3=theta_array[itheta]
    t_theta_1=get_t_from_theta(xij,yij,zij,theta_next_1)
    t_theta_2=get_t_from_theta(xij,yij,zij,theta_next_2)
    t_theta_3=get_t_from_theta(xij,yij,zij,theta_next_3)
    t_theta_all=[t_theta_1[1],t_theta_1[2],t_theta_2[1],t_theta_2[2],t_theta_3[1],t_theta_3[2]]
    itheta_all=[itheta_plus,itheta_minus,itheta]
    t_theta_next=1e99
    iind_theta=0
    for (i,t_theta) in enumerate(t_theta_all)
      if t_theta <= t_theta_next && t_theta > tcur
        t_theta_next=t_theta
        iind_theta=i
      end
    end

    # Next boundary
    t_next=min(t_r_next,t_theta_next)
    ir_next=ir
    itheta_next=itheta
    if t_r_next < t_theta_next
      ir_next=ir_all[round(Int64,(iind_r-1)/2)+1]
    else
      itheta_next=itheta_all[round(Int64,(iind_theta-1)/2)+1]
    end
    (xnext,ynext,znext)=get_xyz_from_xij_t(xij,yij,zij,t_next)
    dist=norml2([xcur-xnext,ycur-ynext,zcur-znext])
    tmid=(tcur+t_next)/2.0
    igrid=get_i_from_rtheta(get_r_from_t(xij,yij,zij,tmid),r_array)
    jgrid=get_i_from_rtheta(get_theta_from_t(xij,yij,zij,tmid),theta_array)
    dens=numberdens[igrid,jgrid]
    if dist > 2*rmax
      dist=0
    end
    
    total+=dens*dist

    xcur=xnext
    ycur=ynext
    zcur=znext
    tcur=t_next
    ir=ir_next
    itheta=itheta_next


  end

  return total

end


# Main loop on pixels
for i=iminplot:imaxplot
  Xij=get_X_from_i(rmax,i,npix)
  println("Computing row $i / $npix")
  if (abs(Xij)<=crop*1.1)
    for j=iminplot:imaxplot
      ipix=(i-1)*npix+j
      Yij=get_X_from_i(rmax,j,npix)
      if (abs(Yij)<=crop*1.1)
      #if (sqrt(Xij^2+Yij^2)<=crop*1.1)
        (xij,yij,zij)=get_xij_from_XY(Xij,Yij,rmax)
        Image[i,j]=ray_trace_integrate(xij,yij,zij,numberdens)*au
      end
    end
  end
end

average_col_dens=sum(Image[iminplot:imaxplot,iminplot:imaxplot],dims=[1,2])/((imaxplot-iminplot+1)^2.0)
println("Average column density in the window = $average_col_dens cm-2")



# Plot the result
using CairoMakie
XX=[get_X_from_i(rmax,i,npix) for i in 1:npix]
YY=[get_X_from_i(rmax,i,npix) for i in 1:npix]
fig=Figure(fontsize=22,size=(800,600),cscale=log10)
ax=Axis(fig[1,1])
hm=heatmap!(ax,XX[iminplot:imaxplot],YY[iminplot:imaxplot],Image[iminplot:imaxplot,iminplot:imaxplot])#, colorrange=(18,22.5))
Colorbar(fig[:,end+1],hm, label="Column density (cm-2) (log)")
save("map_col_dens_$species.png",fig)




