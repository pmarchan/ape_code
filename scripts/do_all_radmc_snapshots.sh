#!/bin/bash

# Enter the number of CPU here !
Ncpu=
################################


task(){
  cd $1
  echo $1
  radmc3d mctherm
  cd ..
}


(
for dir in run_*
do
  ((i=i%Ncpu)); ((i++==0)) && wait
  task $dir &
done
)
