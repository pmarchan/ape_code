# Plot temperature map from radmc-3d output

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import sys

spec1=sys.argv[1:][0]

# To chose
rmaxplot=200


# Read data
f=open("output_ape_sph.dat","r")
params=f.read().split("\n")
f.close()

# Read parameters
loggrid=int(data[0].split(" -- ")[0])
rad_min=float(data[1].split(" -- ")[0])
rad_max=float(data[2].split(" -- ")[0])
nrad=int(data[3].split(" -- ")[0])
theta_min=float(data[4].split(" -- ")[0])
theta_max=float(data[5].split(" -- ")[0])
ntheta=int(data[6].split(" -- ")[0])
nvar=13

f=open("gas_temperature.dat","r")
data_t=f.read().split("\n")
data_t.pop(-1)
f.close()

f=open("abundance_"+spec1+".inp","r")
data_d=f.read().split("\n")
data_d.pop(-1)
data_d.pop(1)
data_d.pop(1)
f.close()

# Build grid
theta=np.linspace(np.pi/2.0-theta_min,np.pi/2.0-theta_max,ntheta)
if loggrid:
  r=np.logspace(np.log10(rad_min),np.log10(rad_max),nrad)
else:
 r=np.linspace(rad_min,rad_max,nrad)
R,THETA=np.meshgrid(r,theta)
TEMP=np.zeros((ntheta,nrad))
DENS=np.zeros((ntheta,nrad))
sphere_t=np.zeros(nr*ntheta)
sphere_d=np.zeros(nr*ntheta)

# Put data in array
for i,dat in enumerate(data_t):
  sphere_t[i]=float(dat)
for i,dat in enumerate(data_d):
  sphere_d[i]=float(dat)

# Read density
for i in range(ntheta):
  TEMP[:][i]=sphere_t[i*nrad:(i+1)*nrad]
  DENS[:][i]=sphere_d[i*nrad:(i+1)*nrad]

# Plot things
plt.rcParams.update({'font.size': 20})
plt.figure(figsize=(12,9))
ax1=plt.subplot(111,projection='polar')
ax1.set_thetamin(0)
ax1.set_thetamax(90)
ax1.set_title(spec1)
im=ax1.pcolormesh(THETA,R,DENS,cmap='inferno',shading="auto",norm=matplotlib.colors.LogNorm())
plt.axis([np.pi/2.0-theta_max,np.pi/2.0-theta_min,0,rmaxplot])
cbar=plt.colorbar(im,ax=ax1,fraction=0.046,pad=0.06)
cbar.set_label("Abundance / H",rotation=270,labelpad=20)

cc=ax1.contour(THETA,R,TEMP,[10,20,30,50,75,100,300],colors="k")
ax1.clabel(cc, inline=True,fmt='%d')

ax1.text(np.radians(-10),ax1.get_rmax()/2.,'r (au)',ha='center',va='center')

plt.gca().set_rmin(0.0)

plt.savefig("map_"+spec1+".png")
