#~/bin/bash

# Enter the number of CPU here !
Ncpu=
################################


task(){
  cd $1
  echo $1
  cp ../../Nautilus_files/* .
  ./nautilus > log
  # Uncomment this line to cleanup the output files on the go
  #rm -rf *.out
  ###########################################################
  cd -
}


(
for dir in particles/part_*
do
  ((i=i%Ncpu)); ((i++==0)) && wait
  task $dir &
done
)
