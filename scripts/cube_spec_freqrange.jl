##################### PARAMETERS ##################################
# This script setup everything you need to make a synthetic observation with imager
# It :
# - Selects transitions of the species you choose in the frequency range you choose
# - Runs RADMC-3D to generate emission maps
# - Converts the emission maps into a data-cube in FITS format for each line
# - Generate an Imager script to be run directly in Imager for each line
# - Everything is stored in a separate directory
#
# What you need to do :
# - Choose the observation parameters below
# - Run > julia cube_spec_freqrange.jl
# - Go to the created directory
# - Run the script in imager for each line (> @script_name.ima)
#
# More information at the botton of the README.


# Parameters
const species="CH3OH"
const NumberdensFileDir="../numberdens/"
const SimulationDirectorySuffix="_incl_0"
const FitsImage="Image_$species"*"_0.fits"
const FreqMinGHz=234.6 #GHz
const FreqMaxGHz=234.75 #GHz
const FreqResolGHz=488e-6 #GHz
const aij_thresh_min=1e-5 #s-1
const DeltaKms=10 # Spectral width to compute around each line
const Inclination=0 #degrees
const vlsr=0.0 # km/s
const dist_pc=140
const NoiseAve=1e-6 #Jy
const RAcoord="24:00:00"
const DECcoord="-35:00:00"
const Antenna=12 #m
const ALMAconfig=[5]
const ALMAtimeHours=[0.5]
const baseline=true

# Computation
const nproc=1 # Number of processors
const FactPixPerBeam=16 # To determine the size of the RADMC-3D pixels compared to the beam

##################################################################


# CONSTANTS
const clight=2.99792458e10
const kb=1.38e-16
const pc=3.08e18
const au=1.5e13

# Packages
using CFITSIO
using Printf

# Possible configurations
PossibleConfigs=[[4,1],[5,2],[6,3],[7,4],[8,5],[9,6]]
PairingRatio=[0.34,0.26,0.25,0.23,0.22,0.21]
ConfigBeamArcsec=[3.4,2.3,1.4,0.92,0.54,0.31,0.21,0.096]

# Coordinates
DECdigits=parse.(Float64,split(DECcoord,":"))
DECdegree=DECdigits[1]+DECdigits[2]/60+DECdigits[3]/3600
const DEC=@sprintf("%.2f",DECdegree)

file=open("amr_grid.inp","r")
dataamr=readlines(file)[7]
close(file)
const boxsize=[parse(Float64,x[1]) for x in collect(eachmatch(r"(\d\.*\d*+E*[\+\-]*\d*+)",dataamr))][end]/au


# This function checks if the required configurations are allowed by ALMA
function check_configs()

  if size(ALMAconfig)[1] < size(ALMAtimeHours)[1]
    println("Number of configurations inconsistent with number of times !")
    return "Error"
  end
  
  ALMAtimeHours_def=copy(ALMAtimeHours)
  if size(ALMAconfig)[1] > 1
    revconfig=sort(ALMAconfig,rev=true)
    if revconfig in PossibleConfigs
      ind=findfirst(x->x==revconfig,PossibleConfigs)
      ratio=PairingRatio[ind]
      ALMAtimeHours_def=[ALMAtimeHours[1],ALMAtimeHours[1]*ratio]
      println("Configuration allowed, rescaling C-",revconfig[2]," time to a factor $ratio w.r.t. C-",revconfig[1]," time")
    else
      println("Configuration not allowed !")
      println("The only possible configurations are")
      for conf in PossibleConfigs
        println(conf)
      end
      return "Error"
    end
  end
  return ALMAtimeHours_def
end

# Get frequency in GHz from the linelist file
function get_freqGHz_linelist(linelist)
  Wavel=parse(Float64,[x[1] for x in collect(eachmatch(r"(\d\.*\d*+E*[\+\-]*\d*+)",linelist))][2])
  return clight/(Wavel/1e4)/1e9
end

# Get frequency in GHz from the linelist file
function get_aij_linelist(linelist)
  aij=parse(Float64,[x[1] for x in collect(eachmatch(r"(\d\.*\d*+E*[\+\-]*\d*+)",linelist))][3])
  return aij
end

# Get the lines in the frequency range
function get_lines(spec,fmin,fmax)
  LinelistFileName="linelist_$spec.inp"
  if !isfile(LinelistFileName)
    println("No linelist file for species $spec !")
    return "Error"
  end
  file=open(LinelistFileName,"r")
  data=readlines(file)
  close(file)
  nPartFunc=parse(Int64,data[14])
  nLines=parse(Int64,data[15+nPartFunc+2])
  iLines=Int64[]
  FreqLines=Float64[]
  for i=1:nLines
    FreqGHz=get_freqGHz_linelist(data[15+nPartFunc+3+i])
    aij=get_aij_linelist(data[15+nPartFunc+3+i])
    if fmin < FreqGHz < fmax && aij>aij_thresh_min
      push!(iLines,i)
      push!(FreqLines,FreqGHz)
    end
  end
  return (iLines,FreqLines)
end

# Convert DeltaF into DeltaV
function deltafGHz_to_vkms(freq,fref)
  return (freq-fref)/fref*clight/1e5
end


# Setup lines to be observed
function get_linelist()

  lines=get_lines(species,FreqMinGHz,FreqMaxGHz)
  if lines=="Error"
    return "Error"
  end
  (iLine_list,FreqLineGHz_list)=lines
  if size(iLine_list)[1]==0
    println("No line found in frequency range !")
    return "Error"
  end
  
  println("Lines # ",iLine_list, " at frequencies ",FreqLineGHz_list," GHz")
  
  file_lines=open("lines.inp","w")
  write(file_lines,"2\n")
  write(file_lines,"1\n")
  write(file_lines,"$species linelist 0 0 0")
  close(file_lines)

  # Regroup lines in the same window if possible
  safe_margin_kms=2.0
  line_groups=[]
  line_refs=[]
  iline_ref=1
  cur_group=[1]
  for (i,f) in enumerate(FreqLineGHz_list)
    if i>1
      deltav=deltafGHz_to_vkms(f,FreqLineGHz_list[iline_ref])
      if abs(deltav) < DeltaKms-safe_margin_kms
        push!(cur_group,i)
        deltav_max=deltafGHz_to_vkms(f,FreqLineGHz_list[cur_group[1]])
        if abs(deltav_max) < DeltaKms-safe_margin_kms
          iline_ref=i
        end
      else
        push!(line_groups,iLine_list[cur_group])
        push!(line_refs,iline_ref)
        cur_group=[i]
        iline_ref=i
      end
    end
  end
  push!(line_groups,iLine_list[cur_group])
  push!(line_refs,iline_ref)

  return (iLine_list[line_refs],line_groups,FreqLineGHz_list[line_refs])
  
end


function print_info(nlines)

  iconfig=maximum(ALMAconfig)
  beamarcsec=ConfigBeamArcsec[iconfig]*100/(0.5*(FreqMinGHz+FreqMaxGHz))
  beamAU=beamarcsec*dist_pc
  npix=round(Int64,FactPixPerBeam*boxsize*2/beamAU)+1

  println("")
  println("**************** Map resolution ******************")
  pixelau_f=@sprintf("%.1f",boxsize/npix*2)
  beamAU_f=@sprintf("%.1f",beamAU)

  println("Beam size for configuration $iconfig = $beamarcsec'' = $beamAU_f au")
  println("RADMC-3D map : $npix pixels of $pixelau_f au")
  
  fhalfwidth=0.5*(FreqMinGHz+FreqMaxGHz)*1e9*DeltaKms*1e5/clight
  nlambda=round(Int64,2.0*fhalfwidth/(FreqResolGHz*1e9))
  diskspace=@sprintf("%.4f",160000*(npix/100)^2.0*nlambda*nlines/1e6)
  println("Disk space needed for emission maps ~ $diskspace Mo")
  println("")

  return npix

end


function make_directory()
  FreqAve=(FreqMaxGHz+FreqMinGHz)/2.0
  FreqPrint=@sprintf("%.3f",FreqAve)
  directory_name0=species*"_$FreqPrint"*"GHz"*SimulationDirectorySuffix
  directory_name=directory_name0
  idir=1
  while isdir(directory_name)
    idir+=1
    directory_name=directory_name0*"_$idir"
  end
  run(`mkdir $directory_name`)
  println("")
  println("Creating directory $directory_name")
  println("")
  return directory_name
end


# End of initialisation
####################################################################

function make_image(iline,freqline,resol,widthkms,npix,line_group)
  fhalfwidth=freqline*1e9*widthkms*1e5/clight
  nlambda=round(Int64,2.0*fhalfwidth/(resol*1e9))+1
  logfile="log"
  nlogs=1
  while isfile(logfile)
    nlogs+=1
    logfile="log_$nlogs"
  end
  if length(line_group)==1
    println("Line $iline, f=$freqline GHz, channels = $nlambda   log file = $logfile")
  else
    firstline=line_group[1]
    lastline=line_group[end]
    println("Lines $firstline to $lastline, f=$freqline GHz, channels = $nlambda    log file = $logfile")
  end
  success=false
  iter=0
  while !success
    success=true
    iter+=1
    run(`radmc3d image iline $iline linenlam $nlambda incl $Inclination npix $npix widthkms $widthkms setthreads $nproc \> $logfile`)
    if occursin("Done...", read(pipeline(`tail -n1 $logfile`), String))
      println("Success on attempt $iter")
    else
      success=false
      if iter>=10
        println("RADMC-3D did not suceed after 10 attempts, please check the log file")
        break
      end
    end
  end
  return success
end


function convert_to_fits(FitsName,npix)
  run(`touch $FitsName`)
  run(`rm $FitsName`)
  RadmcOutputFile=open("image.out","r")
  WholeData=readlines(RadmcOutputFile)
  close(RadmcOutputFile)
  Npixx=parse(Int64,split(lstrip(WholeData[2])," ")[1])
  Npixy=parse(Int64,split(rstrip(WholeData[2])," ")[end])
  SizepixX=parse(Float64,split(lstrip(WholeData[4])," ")[1])
  SizepixY=parse(Float64,split(rstrip(WholeData[4])," ")[end])
  nlams=parse(Int64,WholeData[3])
  minlam=parse(Float64,WholeData[5])
  maxlam=parse(Float64,WholeData[4+nlams])
  if nlams==1
    width_kms=1.0
    deltav_kms=1.0
  else
    width_kms=abs((maxlam-minlam)/(minlam+maxlam)*2.0)*clight/1e5
    deltav_kms=width_kms/(nlams-1)
  end

  Frequency=clight/((maxlam+minlam)/2.0/1e4)
  Beam=1.22*clight/Frequency/1e9/Antenna/1e2

  # Image
  JyFactor=1e23*SizepixX*SizepixY/(dist_pc*pc)^2.0
  ImageData=WholeData[6+nlams:end-1]
  Image3D=Array{Float64,3}(undef,Npixx,Npixy,nlams)
  for k=1:nlams
    for i=1:Npixy
      Image3D[1:Npixx,i,k]=parse.(Float64,ImageData[k+Npixx*(i-1)+(k-1)*Npixx*Npixy:k-1+Npixx*i+(k-1)*Npixx*Npixy])*JyFactor
    end
  end
  bmin=minimum(Image3D)
  bmax=maximum(Image3D)
  noise=NoiseAve
  #noise=bmax/SNratio
  FitsFile=fits_create_file("$FitsName")
  fits_create_img(FitsFile,Image3D)
  fits_write_pix(FitsFile,Image3D)

  # Coordinates
  RAdigits=parse.(Float64,split(RAcoord,":"))
  RAdegree=(RAdigits[1]+RAdigits[2]/60+RAdigits[3]/3600)/24*360
  RAresol=SizepixX/au/dist_pc/3600
  DECdigits=parse.(Float64,split(DECcoord,":"))
  DECdegree=DECdigits[1]+DECdigits[2]/60+DECdigits[3]/3600
  DECresol=SizepixY/au/dist_pc/3600
  fits_write_key(FitsFile, "EQUINOX",2.000000000000E+03,"")
  fits_write_key(FitsFile, "CTYPE1","RA---TAN","")
  fits_write_key(FitsFile, "CRVAL1", RAdegree,"")
  fits_write_key(FitsFile, "CDELT1", RAresol,"")
  fits_write_key(FitsFile, "CRPIX1", npix/2,"")
  fits_write_key(FitsFile, "CUNIT1","deg     ","")
  fits_write_key(FitsFile, "CTYPE2","DEC--TAN","")
  fits_write_key(FitsFile, "CRVAL2", DECdegree,"")
  fits_write_key(FitsFile, "CDELT2", DECresol,"")
  fits_write_key(FitsFile, "CRPIX2", npix/2,"")
  fits_write_key(FitsFile, "CUNIT2", "deg     ","")
  fits_write_key(FitsFile, "CTYPE3", "VRAD","")
  fits_write_key(FitsFile, "CRVAL3", vlsr,"")
  fits_write_key(FitsFile, "CDELT3", deltav_kms,"")
  fits_write_key(FitsFile, "CRPIX3", nlams/2,"")
  fits_write_key(FitsFile, "CUNIT3","km/s","")
  fits_write_key(FitsFile, "RADESYS","FK5","")
  fits_write_key(FitsFile, "RA",RAdegree,"")
  fits_write_key(FitsFile, "DEC",DECdegree,"")
  fits_write_key(FitsFile, "LINE","","")
  fits_write_key(FitsFile, "RESTFREQ",Frequency,"")
  fits_write_key(FitsFile, "VELO-LSR",vlsr,"")

  # Other
  fits_write_key(FitsFile, "OBJECT","RADMC output","SOURCE  NAME")
  fits_write_key(FitsFile, "TIMESYS","UTC     '","")
  fits_write_key(FitsFile, "INSTRUME","test    '","")

  # Flux
  fits_write_key(FitsFile, "BSCALE",1.000000000000E+00,"PHYSICAL = PIXEL*BSCALE + BZERO")
  fits_write_key(FitsFile, "BZERO",0.000000000000E+00,"")
  fits_write_key(FitsFile, "BUNIT","Jy/pixel","Brightness (pixel) unit")
  fits_write_key(FitsFile, "BTYPE","Intensity","")
  fits_write_key(FitsFile, "DATAMIN",bmin,"")
  fits_write_key(FitsFile, "DATAMAX",bmax,"")

  # Restoring beam
  fits_write_key(FitsFile, "BMIN",Beam,"")
  fits_write_key(FitsFile, "BMAJ",Beam,"")
  fits_write_key(FitsFile, "BPA",100.0,"")
  fits_write_key(FitsFile, "END","","")
  fits_close_file(FitsFile)

  return noise

end


function write_gildas_script(OutputFileName,SyntheticFileName,ScriptName,noise,times)
  file_gildas=open(ScriptName,"w")
  gdfname=OutputFileName[1:end-4]*"gdf"
  config=""
  hours=""
  for (i,c) in enumerate(ALMAconfig)
    config=config*"$c "
    h=times[i]
    hours=hours*"$h "
  end
  write(file_gildas,"!\n")
  write(file_gildas,"fits $OutputFileName to $gdfname\n")
  write(file_gildas,"simulate /model $gdfname $DEC /array alma /conf $config /hour $hours /noise $noise\n")
  write(file_gildas,"simulate setup\n")
  write(file_gildas,"simulate coverage\n")
  write(file_gildas,"simulate model\n")
  write(file_gildas,"simulate noise\n")
  write(file_gildas,"simulate image\n")
  write(file_gildas,"STAT CLEAN\n")
  write(file_gildas,"write clean clean_$SyntheticFileName fits\n")
  if baseline
    write(file_gildas,"UV_BASELINE /VELOCITY 0 /WIDTH 10 VELOCITY\n")
    write(file_gildas,"uv_map\n")
    write(file_gildas,"clean\n")
    write(file_gildas,"write clean baseline_$SyntheticFileName fits\n")
  end
  close(file_gildas)
end


function launch_simu()
  println("")
  println("Starting simulation...")
  println("")
  ALMAtimes=check_configs()
  if ALMAtimes=="Error"
    return
  end
  lines=get_linelist()
  if lines=="Error"
    return
  end
  (iLine_list,line_groups,FreqLineGHz_list)=lines
  npix=print_info(size(iLine_list)[1])

  if !isfile("$NumberdensFileDir/numberdens_$species.inp")
    println("No numberdens_ file for species $species in directory $NumberdensFileDir !")
    return "Error"
  end
  if (NumberdensFileDir=="." || NumberdensFileDir=="./")
     println("Taking the numberdens_$species.inp from the current directory")
  else
    try 
      run(`cp $NumberdensFileDir/numberdens_$species.inp .`)
    catch
    end
  end


  # We are good, run the simulation 
  directory_name=make_directory()
  ilast=1
  for (i,iline) in enumerate(iLine_list)
    if i>1 && abs(deltafGHz_to_vkms(FreqLineGHz_list[i],FreqLineGHz_list[ilast])) < DeltaKms/2.0
      println("Line $iline   f=",FreqLineGHz_list[i]," GHz included in previous window")
    else
      ilast=i
      success=make_image(iline,FreqLineGHz_list[i],FreqResolGHz,DeltaKms,npix,line_groups[i])
      if !success
        return false
      end
      FitsName=FitsImage[1:end-5]*"_$i.fits"
      noise=convert_to_fits(FitsName,npix)
      SyntheticFileName="synthetic_$FitsName"
      ScriptName="script_$species"*"_$i.ima"
      write_gildas_script(FitsName,SyntheticFileName,ScriptName,noise,ALMAtimes)
      run(`mv $FitsName $directory_name/`)
      run(`mv $ScriptName $directory_name/`)
    end
  end
  return true
end


simulation=launch_simu()
