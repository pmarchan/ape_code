#!/bin/bash

# Script to setup the radmc-3d simulations of many snapshots to be used later in particle mode

setup_name=cloud_2msol
timestart="0"  # Starting time in yr
timeend="10000"    # Ending time in yr
ntimes=11  # Number of times 


#################################################
dtime=$(echo "($timeend-$timestart)/($ntimes-1)" | bc -l)    # Time interval

# Initialize directory

setup_dir="simu_radmc3d/"$setup_name
mkdir -p simu_radmc3d/
mkdir $setup_dir

#python3 generate_opac.py 

touch radmc3d.inp
echo "nphot = 1000000" >> radmc3d.inp
echo "scattering_mode_max = 1" >> radmc3d.inp
echo "iranfreqmode = 1" >> radmc3d.inp
echo "modified_random_walk = 1" >> radmc3d.inp

cp parameters.nml $setup_dir

touch params_radmc.dat
echo "$timestart" >> params_radmc.dat
echo "$timeend" >> params_radmc.dat
echo "$ntimes" >> params_radmc.dat
res=$(grep "rad_min_au=" parameters.nml)
res2=$(echo "$res" | cut -d '=' -f 2)
echo $res2 >> params_radmc.dat
res=$(grep "rad_max_au=" parameters.nml)
res2=$(echo "$res" | cut -d '=' -f 2)
echo $res2 >> params_radmc.dat
res=$(grep "nrad=" parameters.nml)
res2=$(echo "$res" | cut -d '=' -f 2)
echo $res2 >> params_radmc.dat
res=$(grep "ntheta=" parameters.nml)
res2=$(echo "$res" | cut -d '=' -f 2)
echo $res2 >> params_radmc.dat
res=$(grep "loggrid=" parameters.nml)
res2=$(echo "$res" | cut -d '=' -f 2)
echo $res2 >> params_radmc.dat
mv params_radmc.dat $setup_dir


# Activate RADMC-3D output
Line=$(grep -n -m 1 "radmc_output" parameters.nml | cut -d ':' -f 1)
txt="radmc_output=.true."
sed "${Line}s/.*/$txt/" parameters.nml > parameters.nml.tmp && mv parameters.nml.tmp parameters.nml

# Disable using of RADMC-3D temperature
Line=$(grep -n -m 1 "use_radmc_temp" parameters.nml | cut -d ':' -f 1)
txt="use_radmc_temp=.false."
sed "${Line}s/.*/$txt/" parameters.nml > parameters.nml.tmp && mv parameters.nml.tmp parameters.nml

# Disable particle mode
Line=$(grep -n -m 1 "particle=" parameters.nml | cut -d ':' -f 1)
txt="particle=.false."
sed "${Line}s/.*/$txt/" parameters.nml > parameters.nml.tmp && mv parameters.nml.tmp parameters.nml

# Impose theta min=0 and theta_max=90
Line=$(grep -n -m 1 "theta_min_deg=" parameters.nml | cut -d ':' -f 1)
txt="theta_min_deg=0d0"
sed "${Line}s/.*/$txt/" parameters.nml > parameters.nml.tmp && mv parameters.nml.tmp parameters.nml
Line=$(grep -n -m 1 "theta_max_deg=" parameters.nml | cut -d ':' -f 1)
txt="theta_max_deg=90d0"
sed "${Line}s/.*/$txt/" parameters.nml > parameters.nml.tmp && mv parameters.nml.tmp parameters.nml

# Loop on times and copy APE outputs *inp into each dir.
irun=0

if ! command -v bc >/dev/null 2>&1; then
    echo "Error: 'bc' is not installed. Please install it (e.g., 'brew install bc' on macOS)." >&2
    exit 1
fi

if ! command -v sed >/dev/null 2>&1; then
    echo "Error: 'sed' is not installed. Please install it." >&2
    exit 1
fi

timecur=$(echo "$timestart-$dtime" | bc -l)
Line=$(grep -n -m 1 "time_years" parameters.nml | cut -d ':' -f 1)
for i in `seq 1 $ntimes`
do
  echo $i "/" $ntimes
  i2=$(printf "%03d" $i)
  rundir="$setup_dir/run_$i2"
  mkdir -p $rundir
  timecur=$(echo "$timecur+$dtime" | bc -l)
  txt="time_years=$timecur"
  sed "${Line}s/.*/$txt/" parameters.nml > parameters.nml.tmp && mv parameters.nml.tmp parameters.nml
  ./ape parameters.nml > log
  cp *.inp $rundir
  cp lib/*inp $rundir
done

cp output_ape_sph.dat $setup_dir/

rm *.inp
rm log

echo ""
echo "*************************************************************************************"
echo "WHAT TO DO NEXT ?"
echo "1. Edit scripts/do_all_radmc_snapshots.sh and input the number of CPU you want to use"
echo "2. Go to the directory $setup_dir"
echo "3. Launch ../../scripts/do_all_radmc_snapshots.sh"
echo "Radiative transfer calculations will run on parallel on 1 CPU each."
echo "Count 1 to a few cpu.hour per snapshot"
echo "4. Launch python3 ../../scripts/gather_average_temperature.py"
echo "*************************************************************************************"
echo ""

    
