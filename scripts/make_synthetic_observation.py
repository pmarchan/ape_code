import numpy as np
import os
import re
import subprocess
from astropy.io import fits


##################### PARAMETERS ##################################
# This script setup everything you need to make a synthetic observation with imager
# It :
# - Selects transitions of the species you choose in the frequency range you choose
# - Runs RADMC-3D to generate emission maps
# - Converts the emission maps into a data-cube in FITS format for each line
# - Generate an Imager script to be run directly in Imager for each line
# - Everything is stored in a separate directory
#
# What you need to do :
# - Choose the observation parameters below
# - Run > python3 make_synthetic_observation.py
# - Go to the created directory
# - Run the script in imager for each line (> @script_name.ima)
#
# More information at the botton of the README.


# Parameters
species="CH3OH"
NumberdensFileDir="./"
SimulationDirectorySuffix="_incl_0"
FitsImage="Image_"+species+"_0.fits"
FreqMinGHz=234.6 #GHz
FreqMaxGHz=234.75 #GHz
FreqResolGHz=488e-6 #GHz
AijThreshMin=1e-5 #s-1
EupThreshMax=1e-5 #s-1
DeltaKms=10 # Spectral width to compute around each line
Inclination=0 #degrees
Vlsr=0.0 # km/s
DistPc=140
NoiseAve=1e-6 #Jy
RAcoord="24:00:00"
DECcoord="-35:00:00"
ALMAconfig=[5]
ALMAtimeHours=[0.5]
baseline=True

# Computation
nproc=1 # Number of processors
FactPixPerBeam=16 # To determine the size of the RADMC-3D pixels compared to the beam

##################################################################


# CONSTANTS
CLIGHT=2.99792458e10
HH=6.63e-27
KB=1.38e-16
PC=3.08e18
AU=1.5e13
ANTENNA=12 #m

# Possible configurations
PossibleConfigs=[[4,1],[5,2],[6,3],[7,4],[8,5],[9,6]]
PairingRatio=[0.34,0.26,0.25,0.23,0.22,0.21]
ConfigBeamArcsec=[3.4,2.3,1.4,0.92,0.54,0.31,0.21,0.096]

# Coordinates
DECdigits=DECcoord.split(":")
DECdegree=float(DECdigits[0])+float(DECdigits[1])/60+float(DECdigits[2])/3600
DEC=round(DECdegree,2)

# Grid data
f=open("amr_grid.inp","r")
dataamr_all=f.read().split("\n")
dataamr=dataamr_all[6]
f.close()
boxsize=float(re.findall("(\d\.*\d*E[\+\-]*\d*)",dataamr)[-1])/AU


# Check consistency of configurations
def check_configs():
  NumberOfConfigs=np.size(ALMAconfig)
  NumberOfTimes=np.size(ALMAtimeHours)
  if NumberOfConfigs < NumberOfTimes:
    print("Number of configurations inconsistent with number of times !")
    return "Error"

  ALMAtimeHours_recommended=[x for x in ALMAtimeHours]
  if NumberOfConfigs > 1:
    Reverse_config=ALMAconfig.sort(reverse=True)
    if Reverse_config in PossibleConfigs:
      index_config=PossibleConfigs.index[Reverse_config]
      ratio=PairingRatio[index_config]
      max_time=np.max(ALMAtimeHours)
      ALMAtimeHours_recommended=[max_time,max_time*ratio]
      print("Configuration allowed, rescaling C-"+str(Reverse_config[1])+" time to a factor "+str(ratio)+" w.r.t. C-"+str(Reverse_config[0])+" time.")
    else:
      print("Configuration not allowed !")
      print("The only possible configurations are")
      for conf in PossibleConfigs:
        print(conf)
      return "Error"

  return ALMAtimeHours_recommended


# Get frequency in GHz from the linelist file
def get_freqGHz_linelist(linelist):
  Wavel=float(re.findall("(\d\.*\d*E[\+\-]*\d*)",linelist)[0])
  return CLIGHT/(Wavel/1e4)/1e9

# Get frequency in GHz from the linelist file
def get_aij_linelist(linelist):
  aij=float(re.findall("(\d\.*\d*E[\+\-]*\d*)",linelist)[1])
  return aij

# Get frequency in GHz from the linelist file
def get_aij_linelist(linelist):
  eup=float(re.findall("(\d\.*\d*E[\+\-]*\d*)",linelist)[1])*HH*CLIGHT/KB
  return eup

# Get the lines in the frequency range
def get_lines(spec,fmin,fmax):
  LinelistFileName="linelist_"+spec+".inp"
  if not os.path.isfile(LinelistFileName):
    print("No linelist file for species "+spec+" !")
    return "Error"
  f=open(LinelistFileName,"r")
  data=f.read().split("\n")
  data.pop(-1) # The last line is empty
  f.close()
  nPartFunc=int(data[13])
  nLines=int(data[14+nPartFunc+2])
  iLines=[]
  FreqLines=[]
  for i in range(nLines):
    FreqGHz=get_freqGHz_linelist(data[15+nPartFunc+3+i])
    aij=get_aij_linelist(data[15+nPartFunc+3+i])
    eup=get_eup_linelist(data[15+nPartFunc+3+i])
    if fmin < FreqGHz < fmax and aij>AijThreshMin and eup<EupThreshMax:
      iLines.append(i)
      FreqLines.append(FreqGHz)
  return (iLines,FreqLines)


# Convert DeltaF into DeltaV
def deltafGHz_to_vkms(freq,fref):
  return (freq-fref)/fref*CLIGHT/1e5


# Setup lines to be observed
def get_linelist():

  lines=get_lines(species,FreqMinGHz,FreqMaxGHz)
  if lines=="Error":
    return "Error"
  (iLine_list,FreqLineGHz_list)=lines
  if np.size(iLine_list)==0:
    print("No line found in frequency range !")
    return "Error"

  print("Lines # ",[x+1 for x in iLine_list])
  print(" at frequencies ",FreqLineGHz_list," GHz")

  # Write lines.inp file
  f=open("lines.inp","w")
  f.write("2\n")
  f.write("1\n")
  f.write(species+" linelist 0 0 0")
  f.close()

  # Regroup lines in the same window if possible
  safe_margin_kms=2.0
  line_groups=[]
  line_refs=[]
  iline_ref=0
  cur_group=[0]
  for i,f in enumerate(FreqLineGHz_list):
    if i>0:
      deltav=deltafGHz_to_vkms(f,FreqLineGHz_list[iline_ref])
      if abs(deltav) < DeltaKms-safe_margin_kms:
        cur_group.append(i)
        deltav_max=deltafGHz_to_vkms(f,FreqLineGHz_list[cur_group[1]])
        if abs(deltav_max) < DeltaKms-safe_margin_kms:
          iline_ref=i
      else:
        line_groups.append([iLine_list[x]+1 for x in cur_group])
        line_refs.append(iline_ref)
        cur_group=[i]
        iline_ref=i
  line_groups.append([iLine_list[x]+1 for x in cur_group])
  line_refs.append(iline_ref)

  return ([iLine_list[x]+1 for x in line_refs],line_groups,[FreqLineGHz_list[x] for x in line_refs])
  

    
# Print information on screen
def print_info(nlines):

  iconfig=np.max(ALMAconfig)
  beamarcsec=ConfigBeamArcsec[iconfig-1]*100.0/(0.5*(FreqMinGHz+FreqMaxGHz))
  beamAU=beamarcsec*DistPc
  npix=round(FactPixPerBeam*boxsize*2/beamAU)+1

  print("")
  print("**************** Map resolution ******************")
  pixelau_f=round(boxsize/npix*2,1)
  beamAU_f=round(beamAU,1)

  print("Beam size for configuration "+str(iconfig)+" = "+str(beamarcsec)+"'' = "+str(beamAU_f)+" au")
  print("RADMC-3D map : "+str(npix)+" pixels of "+str(pixelau_f)+" au")
  
  fhalfwidth=0.5*(FreqMinGHz+FreqMaxGHz)*1e9*DeltaKms*1e5/CLIGHT
  nlambda=int(2.0*fhalfwidth/(FreqResolGHz*1e9))
  #diskspace=round(160000*(npix/100)^2.0*nlambda*nlines/1e6,4)
  #print("Disk space needed for emission maps ~ "+str(diskspace)+" Mo")
  print("")

  return npix

# Make directory in which the simulation will be stored
def make_directory():
  FreqAve=(FreqMaxGHz+FreqMinGHz)/2.0
  FreqPrint=round(FreqAve,3)
  directory_name0=species+"_"+str(FreqPrint)+"GHz"+SimulationDirectorySuffix
  directory_name=directory_name0
  idir=1
  while os.path.isdir(directory_name):
    idir+=1
    directory_name=directory_name0+"_"+str(idir)
  os.system("mkdir "+directory_name)
  print("")
  print("Creating directory "+directory_name)
  print("")
  return directory_name



# End of initialisation
####################################################################

def make_image(iline,freqline,resol,widthkms,npix,line_group):
  fhalfwidth=freqline*1e9*widthkms*1e5/CLIGHT
  nlambda=int(2.0*fhalfwidth/(resol*1e9))+1
  logfile="log"
  nlogs=1
  while os.path.isfile(logfile):
    nlogs+=1
    logfile="log_"+str(nlogs)

  if np.size(line_group)==1:
    print("Line "+str(iline)+", f="+str(freqline)+" GHz, channels = "+str(nlambda)+"   log file = "+logfile)
  else:
    firstline=line_group[0]
    lastline=line_group[-1]
    print("Lines "+str(firstline)+" to "+str(lastline)+", f="+str(freqline)+" GHz, channels = "+str(nlambda)+"   log file = "+logfile)

  success=False
  iters=0
  while not success:
    success=True
    iters+=1
    os.system("radmc3d image iline "+str(iline)+" linenlam "+str(nlambda)+" incl "+str(Inclination)+" npix "+str(npix)+" widthkms "+str(widthkms)+" setthreads "+str(nproc)+" > "+logfile)
    command="tail -n 1 "+logfile
    console_pipe=subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, )
    console_out=str(console_pipe.communicate()[0])
    if "Done..." in console_out:
      print("Success on attempt "+str(iters))
    else:
      success=False
      if iters>=10:
        print("RADMC-3D did not suceed after 10 attempts, please check the log file")
        break
  return success



# Store RADMC-3D output in a FITS cube
def convert_to_fits(FitsName,npix):
  os.system("touch "+FitsName)
  os.system("rm "+FitsName)
  RadmcOutputFile=open("image.out","r")
  WholeData=RadmcOutputFile.read().split("\n")
  WholeData.pop(-1) # The last line is empty
  RadmcOutputFile.close()

  Npixx=int(re.findall("(\d+)",WholeData[1])[0])
  Npixy=int(re.findall("(\d+)",WholeData[1])[1])
  SizepixX=float(re.findall("(\d+.\d+)",WholeData[3])[0])
  SizepixY=float(re.findall("(\d+.\d+)",WholeData[3])[1])
  nlams=int(WholeData[2])
  minlam=float(WholeData[4])
  maxlam=float(WholeData[3+nlams])
  if nlams==1:
    width_kms=1.0
    deltav_kms=1.0
  else:
    width_kms=abs((maxlam-minlam)/(minlam+maxlam)*2.0)*CLIGHT/1e5
    deltav_kms=width_kms/(nlams-1)

  Frequency=CLIGHT/((maxlam+minlam)/2.0/1e4)
  Beam=1.22*CLIGHT/Frequency/1e9/ANTENNA/1e2

  # Image
  JyFactor=1e23*SizepixX*SizepixY/(DistPc*PC)**2.0
  ImageData=WholeData[5+nlams:-1]
  Image3D=np.zeros((nlams,Npixx,Npixy))
  for k in range (nlams):
    for i in range (Npixy):
      Image3D[k,:,i]=[float(im)*JyFactor for im in ImageData[k+Npixx*i+k*Npixx*Npixy:k+Npixx*(i+1)+k*Npixx*Npixy]]

  bmin=np.min(Image3D)
  bmax=np.max(Image3D)
  noise=NoiseAve


  # FITS Writing

  hdu=fits.PrimaryHDU(data=Image3D)
  hdu.writeto(FitsName)
  hdul = fits.HDUList([hdu])

  # Coordinates
  RAdigits=RAcoord.split(":")
  RAdegree=(int(float(RAdigits[0]))+int(float(RAdigits[1]))/60+int(float(RAdigits[2]))/3600)/24*360
  RAresol=SizepixX/AU/DistPc/3600
  DECdigits=DECcoord.split(":")
  DECdegree=int(float(DECdigits[0]))+int(float(DECdigits[1]))/60+int(float(DECdigits[2]))/3600
  DECresol=SizepixY/AU/DistPc/3600
  hdul[0].header["EQUINOX"]=(2.000000000000E+03,"")
  hdul[0].header["CTYPE1"]=("RA---TAN","")
  hdul[0].header["CRVAL1"]=(RAdegree,"")
  hdul[0].header["CDELT1"]=(RAresol,"")
  hdul[0].header["CRPIX1"]=(npix/2,"")
  hdul[0].header["CUNIT1"]=("deg","")
  hdul[0].header["CTYPE2"]=("DEC--TAN","")
  hdul[0].header["CRVAL2"]=(DECdegree,"")
  hdul[0].header["CDELT2"]=(DECresol,"")
  hdul[0].header["CRPIX2"]=(npix/2,"")
  hdul[0].header["CUNIT2"]=("deg","")
  hdul[0].header["CTYPE3"]=("VRAD","")
  hdul[0].header["CRVAL3"]=(Vlsr,"")
  hdul[0].header["CDELT3"]=(deltav_kms,"")
  hdul[0].header["CRPIX3"]=(nlams/2,"")
  hdul[0].header["CUNIT3"]=("km/s","")
  hdul[0].header["RADESYS"]=("FK5","")
  hdul[0].header["RA"]=(RAdegree,"")
  hdul[0].header["DEC"]=(DECdegree,"")

  # Other
  hdul[0].header["LINE"]=("","")
  hdul[0].header["RESTFREQ"]=(Frequency,"")
  hdul[0].header["VELO-LSR"]=(Vlsr,"")
  hdul[0].header["OBJECT"]=("RADMC output","SOURCE  NAME")
  hdul[0].header["TIMESYS"]=("UTC","")
  hdul[0].header["INSTRUME"]=("test","")
  # Flux
  hdul[0].header["BSCALE"]=(1.000000000000E+00,"PHYSICAL = PIXEL*BSCALE + BZERO")
  hdul[0].header["BZERO"]=(0.000000000000E+00,"")
  hdul[0].header["BUNIT"]=("Jy/pixel","Brightness (pixel) unit")
  hdul[0].header["BTYPE"]=("Intensity","")
  hdul[0].header["DATAMIN"]=(bmin,"")
  hdul[0].header["DATAMAX"]=(bmax,"")

  # Beam
  hdul[0].header["BMIN"]=(Beam,"")
  hdul[0].header["BMAJ"]=(Beam,"")
  hdul[0].header["BPA"]=(100.0,"")
  hdul.writeto(FitsName,overwrite=True)

  return noise


# Writing GILDAS script for Imager
def write_gildas_script(OutputFileName,SyntheticFileName,ScriptName,noise,times):
  file_gildas=open(ScriptName,"w")
  gdfname=OutputFileName[0:-5]+".gdf"
  config=""
  hours=""
  for (i,c) in enumerate(ALMAconfig):
    config=config+str(c)+" "
    h=times[i]
    hours=hours+str(h)+" "

  file_gildas.write("!\n")
  file_gildas.write("fits "+OutputFileName+" to "+gdfname+"\n")
  file_gildas.write("simulate /model "+gdfname+" "+str(DEC)+" /array alma /conf "+str(config)+" /hour "+str(hours)+" /noise "+str(noise)+"\n")
  file_gildas.write("simulate setup\n")
  file_gildas.write("simulate coverage\n")
  file_gildas.write("simulate model\n")
  file_gildas.write("simulate noise\n")
  file_gildas.write("simulate image\n")
  file_gildas.write("STAT CLEAN\n")
  file_gildas.write("write clean clean_"+SyntheticFileName+" fits\n")
  if baseline:
    file_gildas.write("UV_BASELINE /VELOCITY 0 /WIDTH 10 VELOCITY\n")
    file_gildas.write("uv_map\n")
    file_gildas.write("clean\n")
    file_gildas.write("write clean baseline_"+SyntheticFileName+" fits\n")
  file_gildas.close()


# Main function with checks
def launch_simu():
  print("")
  print("Starting simulation...")
  print("")
  ALMAtimes=check_configs()
  if ALMAtimes=="Error":
    return
  lines=get_linelist()
  if lines=="Error":
    return
  (iLine_list,line_groups,FreqLineGHz_list)=lines
  npix=print_info(np.size(iLine_list))

  if not os.path.isfile(NumberdensFileDir+"/numberdens_"+species+".inp"):
    print("No numberdens_ file for species "+species+" in directory "+NumberdensFileDir+" !")
    return "Error"
  if NumberdensFileDir=="." or NumberdensFileDir=="./":
     print("Taking the numberdens_"+species+".inp from the current directory")
  else:
    try: 
      os.system("cp "+NumberdensFileDir+"/numberdens_"+species+".inp .")
    except:
      print("No numberdens_ file for species "+species+" in directory "+NumberdensFileDir+" !")

    

  # We are good, run the simulation 
  directory_name=make_directory()
  ilast=0
  for (i,iline) in enumerate(iLine_list):
    if i>1 and abs(deltafGHz_to_vkms(FreqLineGHz_list[i],FreqLineGHz_list[ilast])) < DeltaKms/2.0:
      print("Line $iline   f=",FreqLineGHz_list[i]," GHz included in previous window")
    else:
      ilast=i
      success=make_image(iline,FreqLineGHz_list[i],FreqResolGHz,DeltaKms,npix,line_groups[i])
      if not success:
        return False
      FitsName=FitsImage[0:-5]+"_"+str(i)+".fits"
      noise=convert_to_fits(FitsName,npix)
      SyntheticFileName="synthetic_"+FitsName
      ScriptName="script_"+species+"_"+str(i)+".ima"
      write_gildas_script(FitsName,SyntheticFileName,ScriptName,noise,ALMAtimes)
      os.system("mv "+FitsName+" "+directory_name+"/")
      os.system("mv "+ScriptName+" "+directory_name+"/")
  return True


simulation=launch_simu()
