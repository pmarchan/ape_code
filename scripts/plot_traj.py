# Plot history of particle

import matplotlib.pyplot as plt
import numpy as np




# Read data
f=open("part_traj.dat","r")
data=f.read().split("\n")[1:]
data.pop(-1) # The last line is empty
ntimes=np.size(data)
nvar=10

traj=np.zeros((ntimes,nvar))

# Put data in array
for i, dat in enumerate(data):
  point=[float(x) for x in dat.split(" ") if x.strip()]
  traj[i,0:nvar-1]=point[0:nvar-1]


fig, ax1 = plt.subplots()
plt.yscale("log")
ax2 = ax1.twinx()
plt.yscale("log")
ax1.plot(traj[:,0],traj[:,3]/3.83e-24, color="b")
ax2.plot(traj[:,0],traj[:,4], color="r")
ax2.set_ylim(1e1,1e4)
ax1.set_xlabel("t (year)")
ax1.set_ylabel("Density (cm-3)", color="b")
ax2.set_ylabel("Temperature (K)", color="r")
plt.title("Density and temperature history")
plt.savefig("History.png")

fig, ax3 = plt.subplots()
plt.xscale("log")
plt.yscale("log")
ax4 = ax3.twinx()
plt.yscale("log")
ax3.plot(np.sqrt(np.square(traj[:,1])+np.square(traj[:,2])),traj[:,3]/3.83e-24, color="b")
ax4.plot(np.sqrt(np.square(traj[:,1])+np.square(traj[:,2])),traj[:,4], color="r")
ax4.set_ylim(1e1,1e4)
ax3.set_xlabel("r (au)")
ax3.set_ylabel("Density (cm-3)", color="b")
ax4.set_ylabel("Temperature (K)", color="r")
plt.title("Density and temperature vs radial distance")
plt.savefig("Profile.png")

plt.show()
