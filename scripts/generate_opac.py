# Use DSHARP module to generate opacities for radmc3d


import matplotlib.pyplot as plt
import numpy as np
import os
import pkg_resources
import dsharp_opac as opacity

# get the optical constants

d1n = opacity.diel_henning('waterice',      new=True)
d2n = opacity.diel_henning('olivine',       new=True)
d3n = opacity.diel_henning('orthopyroxene', new=True)
d4n = opacity.diel_henning('organics',      new=True)
d5n = opacity.diel_henning('iron',          new=True)
d6n = opacity.diel_henning('troilite',      new=True)

d1o = opacity.diel_henning('waterice',      new=False)
d2o = opacity.diel_henning('olivine',       new=False)
d3o = opacity.diel_henning('orthopyroxene', new=False)
d4o = opacity.diel_henning('organics',      new=False)
d5o = opacity.diel_henning('iron',          new=False)
d6o = opacity.diel_henning('troilite',      new=False)

p_w   = opacity.diel_pollack1994('water ice')
p_ol  = opacity.diel_pollack1994('olivine')
p_op  = opacity.diel_pollack1994('orthopyroxene')
p_org = opacity.diel_pollack1994('organics')
p_i   = opacity.diel_pollack1994('iron')
p_t   = opacity.diel_pollack1994('troilite')

const_o = [d1o, d2o, d3o, d4o, d5o, d6o]
const_n = [d1n, d2n, d3n, d4n, d5n, d6n]
const_p = [p_w, p_ol, p_op, p_org, p_i, p_t]

datafile = opacity.get_datafile('default_opacities_smooth.npz')
res = np.load(datafile)

a     = res['a']
lam   = res['lam']
k_abs = res['k_abs']
k_sca = res['k_sca']
g     = res['g']
rho_s = res['rho_s']

# Load grain size-distribution sizes from ishinisan-generated file
fd=open("../lib/tablecoag_20bin.dat","r")
fd.readline()
ndist=int(fd.readline().split()[1])
fd.readline()
fd.readline()
atemp=[float(i) for i in fd.readline().split()]
fd.close()
lll=len(atemp)
adist=atemp[1:lll:4]

# Opacities over the size-distribution
lnu=np.size(k_abs,1)
kabs_dist=np.zeros((ndist,lnu))
ksca_dist=np.zeros((ndist,lnu))
gsca_dist=np.zeros((ndist,lnu))

# Interpolate opacities
f1=open("dustopac.inp","w")
f1.write("2\n")
f1.write(str(ndist)+"\n")
f1.write("================================================\n")
ii=-1
for agrain in adist:
  igrain=a.searchsorted(agrain)
  ii=ii+1
  d1=(agrain-a[igrain-1])/(a[igrain]-a[igrain-1])
  if agrain < a[0]:
    d1=1
  kabs_dist[ii,:]=k_abs[igrain,:]*d1+k_abs[igrain-1,:]*(1.0-d1)
  ksca_dist[ii,:]=k_sca[igrain,:]*d1+k_sca[igrain-1,:]*(1.0-d1)
  gsca_dist[ii,:]=g[igrain,:]*d1+g[igrain-1,:]*(1.0-d1)
  # Write data
  biname="bin"+str(ii)
  f1.write("1\n")
  f1.write("0\n")
  f1.write(biname+"\n")
  f1.write("================================================\n")
  # Write data
  filename="dustkappa_"+biname+".inp"
  f=open(filename,"w")
  f.write("3\n")
  f.write(str(lnu)+"\n")
  for ilam in range(lnu):
    f.write('%15.8e    %15.8e    %15.8e    %15.8e\n' % (lam[ilam]*1e4,kabs_dist[ii,ilam],ksca_dist[ii,ilam],gsca_dist[ii,ilam]))
  f.close()

f1.close()





