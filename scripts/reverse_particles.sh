#!/bin/bash

# Reverse the structure_evolution.dat if particle and reverse=true

reverse_structure_evolution () {
  awk '{ lines[NR] = $0 } END { for (i = NR; i > 0; i--) print lines[i] }' "$1" > temp_struc.dat
  rm -f "$1"

  if [ "$(uname)" = "Darwin" ]; then
    # macOS
    sed -i '' '$ d' temp_struc.dat
    sed -i '' '$ d' temp_struc.dat
  else
    # Linux
    sed -i '$ d' temp_struc.dat
    sed -i '$ d' temp_struc.dat
  fi

  rm -f temp2.txt
  touch temp2.txt

  echo " ! (yr)    log(mag)    log(cm-3)   log(K)" >>temp2.txt
  echo " ! time    log(Av)     log(n)      log(T)" >>temp2.txt
  cat temp2.txt temp_struc.dat >> "$1"
  rm -f temp2.txt temp_struc.dat
}


reverse_part_traj () {
  awk '{ lines[NR] = $0 } END { for (i = NR; i > 0; i--) print lines[i] }' "$1" > temp_struc.dat
  rm -f "$1"

  if [ "$(uname)" = "Darwin" ]; then
    # macOS
    sed -i '' '$ d' temp_struc.dat
  else
    # Linux
    sed -i '$ d' temp_struc.dat
  fi

  rm -f temp2.txt
  touch temp2.txt

  echo "Time (yr)        x (au)           y (au)         Density (g/cm3)    Temperature (K)  v_r (cm/s)       v_phi (cm/s)     v_theta (cm/s) Grain index   Loc    Mstar (Msun)     Rstar (rsun)    Lstar (lsun)      Tstar (K)       Mdisk (msun)    Rdisk (au)" >>temp2.txt
  cat temp2.txt temp_struc.dat >> "$1"
  rm -f temp2.txt temp_struc.dat
}

  


if [ $1 -eq "1" ]
then
  for file in particles/part*/structure_evolution.dat
  do
    reverse_structure_evolution $file
  done
else
  reverse_structure_evolution structure_evolution.dat
  reverse_part_traj part_traj.dat
fi

