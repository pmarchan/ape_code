subroutine read_params(paramfile)
  use variables
  implicit none
  character(127), intent(in) :: paramfile


! Read namelist
  namelist/grid_params/rad_min_au,rad_max_au,nrad,theta_min_deg,theta_max_deg,ntheta,loggrid,nrad,radmc_output,nautilus_time_kyr,snapshot_nautilus
  namelist/model_params/Mass,masstoflux,temp_mol_cloud,time_years,t_pstar_age,coagulation,disk_cutoff,alphadisk,dust_to_gas,use_radmc_temp
  namelist/particles/particle,tmax,dt0,dyn_fact,x_ini,z_ini,omega0,radmc_dir,steadystate,reverse,grid_of_part,cloud_extinction
  namelist/override_params/mdisk_over,rdisk_over,rdisk_in_over,mstar_over,rstar_over,tstar_over,lstar_over,outflow_angle_over,override

  open(55,file=trim(adjustl(paramfile)),status="old")
  read(55,NML=grid_params)
  rewind(55)
  read(55,NML=model_params)
  rewind(55)
  read(55,NML=particles)
  rewind(55)
  read(55,NML=override_params)
  close(55)

  if (rad_min_au > rad_max_au) then
    print*, "ERROR : rad_min_au > rad_max_au"
    stop
  end if
  if (theta_min_deg > theta_max_deg) then
    print*, "ERROR : theta_min_deg > theta_max_deg"
    stop
  end if
  theta_min_deg=max(theta_min_deg,0d0)
  theta_max_deg=min(theta_max_deg,90d0)

  if (particle .and. grid_of_part .and. .not. reverse) then
    print*, "ERROR : set reverse=.true. for grid of particles !"
    stop
  end if


  if(.not. particle .or. grid_of_part) then
    nmaxcell=nrad*ntheta

    ! Allocate arrays
    allocate(xcell(nmaxcell,2))
    allocate(rtcell(nmaxcell,2))
    allocate(sizecell(nmaxcell))
    allocate(rhocell(nmaxcell))
    allocate(tcell(nmaxcell))
    allocate(wherecell(nmaxcell))
    allocate(Avcell(nmaxcell))
    allocate(vel(nmaxcell,5))
    allocate(igraincell(nmaxcell))
    allocate(radii(nrad),densities_radii(nrad),vel_radii(nrad),velphi_radii(nrad))

  end if

end subroutine
