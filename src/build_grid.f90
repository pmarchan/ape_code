subroutine build_grid()
  use variables
  implicit none
  integer :: i,j

  ncell=0

  ! Change the resol if the user-defined resolution is smaller than the sampling in normalized radius for the BE sphere
  if (rad_min_au < dx/scale_x/au) then
    rad_min_au = dx/scale_x/au*1.5
    switchresol = .true.
  endif
  theta_min=theta_min_deg*pi/180d0
  theta_max=theta_max_deg*pi/180d0


  ! Build the grid coordinates
  do i=1,nrad
  do j=1,ntheta
    ncell=ncell+1
    if(loggrid) then
      rtcell(ncell,1)= rad_min_au*(rad_max_au/rad_min_au)**(dble(i-1)/dble(nrad-1))
      sizecell(ncell)= (rad_min_au*(rad_max_au/rad_min_au)**(dble(i-0.5)/dble(nrad-1))-rad_min_au*(rad_max_au/rad_min_au)**(dble(i-1.5)/dble(nrad-1)))*au
    else
      rtcell(ncell,1)= rad_min_au+dble(i-1d0)/dble(nrad-1d0)*(rad_max_au-rad_min_au)
      sizecell(ncell)= dble(i-1d0)/dble(nrad-1d0)*(rad_max_au-rad_min_au)
    end if
    if (j==1) radii(i) = rtcell(ncell,1)*au
    rtcell(ncell,2)= theta_min+dble(j-0.5)*(theta_max-theta_min)/dble(ntheta)
    xcell(ncell,1)=rtcell(ncell,1)*dsin(rtcell(ncell,2))
    xcell(ncell,2)=rtcell(ncell,1)*dcos(rtcell(ncell,2))
  end do
  end do

  ! Convert to au
  xcell=xcell*au
  rtcell(:,1)=rtcell(:,1)*au

  
end subroutine
