subroutine initialize()
  use variables
  implicit none
  integer :: i,j
  double precision :: dum
  character(256) :: file_temp
  character(5) :: ich
  logical :: radmc_temp_file_exist



   ! Initialize Bonnor-Ebert sphere
   call initialize_be()
   if (t_pstar_age) time_years=time_years+tff/s2year
   if (time_years*s2year>tff) then
     star=.true.
     disk=.true.
   endif


  ! Initialize particle or grid
  if(particle .and. .not. grid_of_part) then
    dt0=dt0*s2year
    tmax=tmax*s2year
    if (t_pstar_age) tmax=tmax+tff
    x_ini=x_ini*au
    z_ini=z_ini*au
    open(101,file=trim(adjustl(part_file)),status="replace")
    write(101,*)  "Time (yr)        x (au)           y (au)         Density (g/cm3)    Temperature (K)  v_r (cm/s)       v_phi (cm/s)     v_theta (cm/s) Grain index   Loc    Mstar (Msun)     Rstar (rsun)    Lstar (lsun)      Tstar (K)       Mdisk (msun)    Rdisk (au)"

  else 
    call build_grid()
    dt0=dt0*s2year
    tmax=tmax*s2year
    if (t_pstar_age) tmax=tmax+tff
  end if


  ! If using radmc temperature, get info from the radmc snapshots
  if(use_radmc_temp) then
    
    if (particle) then

      if(steadystate) then

        allocate(radmc_temperatures(1,nradradmc,nthetaradmc))
        file_temp=trim(adjustl(radmc_dir))//"temp_radmc/gas_temperature.dat"
        open(203,file=file_temp,status="old")
        do j=1,nradradmc*nthetaradmc
          read(203,*) radmc_temperatures(1,mod(j-1,nradradmc)+1,floor((j-1.0)/nradradmc+1))
        end do
        close(203)

      else

        open(202,file=trim(adjustl(radmc_dir))//"/params_radmc.dat",status="old")
        read(202,*) tminradmc
        read(202,*) tmaxradmc
        read(202,*) nradmc
        read(202,*) rminradmc
        read(202,*) rmaxradmc
        read(202,*) nradradmc
        read(202,*) nthetaradmc
        read(202,*) logradmc
        close(202)
        tminradmc=tminradmc*s2year
        tmaxradmc=tmaxradmc*s2year
        dtradmc=(tmaxradmc-tminradmc)/dble(nradmc-1)
        if(.not. loggrid) drradmc=rmaxradmc/dble(nradradmc)

        allocate(radmc_temperatures(nradmc,nradradmc,nthetaradmc))
        do i=1,nradmc
          write(ich,"(i4)") i
          file_temp=trim(adjustl(radmc_dir))//"temp_radmc/gas_temperature_"//trim(adjustl(ich))//".dat"
          open(203,file=file_temp,status="old")
          do j=1,nradradmc*nthetaradmc
            read(203,*) radmc_temperatures(i,mod(j-1,nradradmc)+1,floor((j-1.0)/nradradmc+1))
          end do
          close(203)
        end do

      endif

    else ! snapshot mode

      inquire(file="gas_temperature.dat",exist=radmc_temp_file_exist)
      if (.not. radmc_temp_file_exist) then
        print*, "No gas_temperature.dat file !"
        print*, "You first need to run radmc3d on the snapshot"
        print*, "1. Set radmc_output=.true."
        print*, "2. Set radmc_temp=.false."
        print*, "3. Run ./ape parameters.nml"
        print*, "4. Run $ radmc3d mctherm (setthreads N)"
        print*, "5. Run python3 scripts/average_temp_radmc3d.py"
        print*, "6. Try again."
        stop
      end if
      open(203,file="gas_temperature.dat",status="old")
      rminradmc=rad_min_au
      rmaxradmc=rad_max_au
      nradradmc=nrad
      nthetaradmc=ntheta
      logradmc=loggrid

      allocate(radmc_temperatures(1,nradradmc,nthetaradmc))
      do j=1,nradradmc*nthetaradmc
        read(203,*) radmc_temperatures(1,mod(j-1,nradradmc)+1,floor((j-1.0)/nradradmc+1))
      end do
      close(203)
      !open(300,file="gas_temperature.dat",status="old")
      !allocate(temp_radmc(ncell))
      !do j=1,ntheta
        !do i=1,nrad
          !read(300,*) temp_radmc(j+(i-1)*ntheta)
        !end do
      !end do
      !close(300)

    end if

  end if


  call initialize_HO09()


  ! Read coagulation table, either Lebreuilly+23 or Marchand+23
  if (.not. dust_to_gas==1d-2) then
    shark_dist=.false.
  endif

  if (shark_dist) then
    ! Lebreuilly+23

    nsize=20
    ngrainpoints=nrhograins
    open(21,file=trim(adjustl(pathlib))//"dust_sizes_Leb23.inp",status="old")
    allocate(grain_size(nsize),grain_abun(nrhograins,nsize),grain_dens(nrhograins,nsize),grain_rho(nrhograins),grain_mass(nsize))
    do i=1,nsize
      read(21,*) grain_size(i)
      grain_mass(i)=4d0/3d0*pi*rhograin*grain_size(i)**3d0
    end do
    close(21)

    open(20,file=trim(adjustl(pathlib))//"coagulation_Leb23.dat",status="old")
    do j=1,nrhograins
      read(20,*) grain_rho(j),grain_dens(j,1:nsize)
      do i=1,nsize
        grain_dens(j,i)=grain_dens(j,i)/grain_rho(j)
        grain_abun(j,i)=grain_dens(j,i)*mean*mp/grain_mass(i)
      end do
    end do
    close(20)

  else
    ! Marchand+23

    open(20,file=trim(adjustl(pathlib))//"coagulation_Mar23.dat",status="old")
    read(20,*)
    read(20,*) nchi, nsize
    read(20,*)
    read(20,*)
    ngrainpoints=nchi
    allocate(tableline(nsize*4))
    allocate(grain_size_chi(nchi,nsize),grain_abun(nchi,nsize),grain_dens(nchi,nsize))
  
    read(20,*) chimin,tableline(:)
    do j=1,nsize
      grain_size_chi(1,j)=tableline(1+(j-1)*4)
      grain_abun(1,j)=tableline(4*j)
      grain_dens(1,j)=tableline(4*j)*tableline(3+(j-1)*4)/(mean*mp)
    end do
    dum=sum(grain_dens(1,:))
    read(20,*) chimin,tableline(:)
    do j=1,nsize
      grain_abun(1,j)=grain_abun(1,j)*dust_to_gas/dum
      grain_dens(1,j)=grain_dens(1,j)*dust_to_gas/dum
      grain_size_chi(2,j)=tableline(1+(j-1)*4)
      grain_abun(2,j)=tableline(4*j)*dust_to_gas/dum
      grain_dens(2,j)=tableline(4*j)*tableline(3+(j-1)*4)/(mean*mp)*dust_to_gas/dum
    end do
    do i=3,nchi
      read(20,*) chimax,tableline(:)
      do j=1,nsize
        grain_size_chi(i,j)=tableline(1+(j-1)*4)
        grain_abun(i,j)=tableline(4*j)*dust_to_gas/dum
        grain_dens(i,j)=tableline(4*j)*tableline(3+(j-1)*4)/(mean*mp)*dust_to_gas/dum
      end do
    end do
    close(20)

  end if




  ! Read opacity table
  if (shark_dist) then
    nopac=nrhograins
    open(20,file=trim(adjustl(pathlib))//"tab_kappas_Leb23.dat",status="old")
  else
    nopac=nchi
    open(20,file=trim(adjustl(pathlib))//"tab_kappas_Mar23.dat",status="old")
  end if
  allocate(opac_table(nopac,ntemp_opac))
  do i=1,nopac
    do j=1,ntemp_opac
      read(20,*) dum,dum,opac_table(i,j)
    end do
  end do
  close(20)

  ! Disk's magnetic field (based on Nakano et al. 2002)
  Bdisk=4.27d-2/masstoflux

  ! Initialize sampling in space for the disk
  do i=1,nprofdisk
    raddisk(i)=rmaxdisk/dble(nprofdisk)*dble(i-0.5)
  end do


end subroutine


! Initialize luminosity from table
subroutine initialize_HO09()
  use variables
  implicit none
  integer :: n,i,imass
  !integer :: imdot, j
  !double precision :: new_mdot_log, new_rad
  double precision :: new_mass, dum
  integer, dimension(5) :: HO09_n_mstar_points                  ! Max number of points per mstar for the HO09 track
  double precision, dimension(:,:,:), allocatable :: HO09_track          ! Hosokawa & Omukai 2009 track for protostar
  double precision :: new_lstar, new_rstar, new_ltot

  ! Reading HO09 track
  open(20,file=trim(adjustl(pathlib))//"Hosokawa_Omukai_2009.dat",status="old")
  read(20,*) HO09_n_mstar_points
  allocate(HO09_track(nmdot_HO09,maxval(HO09_n_mstar_points),6)) ! Mstar, Mdot, Rstar, Rph, L*, Ltot, t(yr)
  do n=1,nmdot_HO09
    do i=1,HO09_n_mstar_points(n)
      read(20,*) dum, HO09_track(n,i,1:6)
    end do
  end do
  close(20)

  HO09_track(:,:,1)=HO09_track(:,:,1)*msun
  HO09_track(:,:,2)=HO09_track(:,:,2)*msun/s2year
  HO09_track(:,:,3)=HO09_track(:,:,3)*rsun
  HO09_track(:,:,4)=HO09_track(:,:,4)*rsun
  HO09_track(:,:,5)=HO09_track(:,:,5)*lsun
  HO09_track(:,:,6)=HO09_track(:,:,6)*lsun

  ! Accretion rates
  HO09_mdot(1)=1d-6/s2year*msun
  HO09_mdot(2)=1d-5/s2year*msun
  HO09_mdot(3)=1d-4/s2year*msun
  HO09_mdot(4)=1d-3/s2year*msun
  HO09_mdot(5)=3d-3/s2year*msun

  ! Min and max masses and radii for resampling
  min_mstar=1d99
  max_mstar=1d-99
  min_rstar=1d99
  max_rstar=1d-99
  do n=1,nmdot_HO09
    min_mstar=min(min_mstar,HO09_track(n,1,1))
    max_mstar=max(max_mstar,HO09_track(n,HO09_n_mstar_points(n),1))
    min_rstar=min(min_rstar,HO09_track(n,1,3))
    max_rstar=max(max_rstar,HO09_track(n,HO09_n_mstar_points(n),3))
  end do

  ! Resampling
  do n=1,nmdot_HO09
    ! In mass
    imass=2
    do i=1,nmass_HO09
      new_mass=min_mstar*(max_mstar/min_mstar)**(dble(i-1)/dble(nmass_HO09-1))
      do while (new_mass > HO09_track(n,imass,1) .and. imass < HO09_n_mstar_points(n))
        imass=imass+1
      end do
      ! Linear Interpolation
      new_rstar=(HO09_track(n,imass,3)*(new_mass-HO09_track(n,imass-1,1)) + HO09_track(n,imass-1,3)*(HO09_track(n,imass,1)-new_mass))/(HO09_track(n,imass,1)-HO09_track(n,imass-1,1))
      new_lstar=(HO09_track(n,imass,5)*(new_mass-HO09_track(n,imass-1,1)) + HO09_track(n,imass-1,5)*(HO09_track(n,imass,1)-new_mass))/(HO09_track(n,imass,1)-HO09_track(n,imass-1,1))
      new_ltot =(HO09_track(n,imass,6)*(new_mass-HO09_track(n,imass-1,1)) + HO09_track(n,imass-1,6)*(HO09_track(n,imass,1)-new_mass))/(HO09_track(n,imass,1)-HO09_track(n,imass-1,1))
      HO09_track_resampled(n,i,1)=max(1d-10,new_rstar)
      HO09_track_resampled(n,i,2)=max(1d-10,new_lstar)
      HO09_track_resampled(n,i,3)=max(1d-10,new_ltot)
    end do
  end do


!  ! Resampling to get mdot as a function of Mstar and Rstar
!  do i=1,nmass_HO09
!    do j=1,nrad_HO09
!      new_mass=min_rstar*(max_rstar/min_rstar)**(dble(i-1)/dble(nrad_HO09-1))
!      new_rad =min_rstar*(max_rstar/min_rstar)**(dble(i-1)/dble(nrad_HO09-1))
!      imdot=2
!      do while (new_rad > HO09_track_resampled(imdot,imass,1) .and. imdot < nmdot_HO09)
!        imdot=imdot+1
!      end do
!      new_mdot_log = 10**( dlog10(HO09_mdot(imdot-1))*(HO09_track_resampled(imdot,imass,1)-new_rad) + dlog10(HO09_mdot(imdot))*(new_rad-HO09_track_resampled(imdot-1,imass,1)) )
!      HO09_mdot_rm(i,j) = new_mdot_log
!    end do
!  end do

 
  deallocate(HO09_track)
end subroutine
