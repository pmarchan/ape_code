subroutine calc_flow(time,xcoord,zcoord,rho,temp,vr,vphi,vtheta,loccell,Av)
  use variables
  implicit none
  double precision, intent(in) :: time,xcoord,zcoord
  double precision :: rho,vr,vphi,vtheta,temp,Av
  double precision,dimension(3) :: dzones,vrzones,vphizones,vthetazones,tempzones
  double precision :: rho1,vr1,vphi1,vtheta1,temp1
  double precision :: vx,vz
  double precision :: r,theta
  integer :: loccell  ! Where is the cell ? 1=envelope, 2=disk, 3=star, 4=outflow
  integer :: idisk ! Index in disk profile
  integer :: izone
  double precision :: interp_temp_radmc
  double precision :: tadjust
  double precision :: rcav
  double precision :: get_outflow_density


  r=dsqrt(xcoord**2d0+zcoord**2d0)
  theta=dabs(datan(xcoord/zcoord))

  ! Calculate flow variables
  ! Envelope by default
  loccell=1

  ! If too close to center, then in star
  if(r<=rdisk_in) then
    loccell=3
    vr=0d0
    vphi=0d0
    vtheta=0d0
    rho=1d-20
    temp=Tstar
    Av=0

  else

    ! Check if in outflow
    tacc=2.0*tff
    rcav=(abs(zcoord)/au/0.191d0)**(2d0/3d0)*(time/tacc)**2d0
    if(star .and. (override .and. outflow_angle_over .ne. 0d0 .and. theta<=outflow_angle_over*pi/180d0 &
      & .or. .not. override .and. r*dsin(theta)/au<=rcav)) then
      loccell=4
      rho=get_outflow_density(zcoord)
      vr=dsqrt(2d0*gg*Mstar/rdisk_in)
      vtheta=0d0
      temp=(Tstar**4d0*(Rstar/r)**2d0+temp_mol_cloud**4d0)**0.25
      vphi=dsqrt(gg*Mstar/rdisk_in)
      Av=1e-2

    ! Not in outflow
    else
        
      ! Check if in disk
      idisk=0
      if(disk .and. r<rmaxdisk) idisk=floor (r/rmaxdisk*nprofdisk+0.5)
      rlimdisk=rmaxdisk
      if (disk_cutoff) rlimdisk=rdisk
      if(idisk .ne. 0) then
        !if(disk .and. r>rdisk_in .and. r<rlimdisk .and.  zcoord<5*hdisk(idisk)  .and. zcoord/r < AspectRatioMax) loccell=2
        if(disk .and. r>rdisk_in .and. r<rlimdisk .and.  zcoord<5*hdisk(idisk)  .and. zcoord/r < AspectRatioMax) then
          !print*, "DISKKKK", r/au,rlimdisk/au,zcoord/au,hdisk(idisk)/au,AspectRatioMax
          loccell=2
        end if
      end if
  
      izone=1
      call calc_BE_flow(time,r,rho1,vr1,vphi1,vtheta1)

      ! Use envelope temperature estimation ?
      temp1=(Tstar**4d0*(Rstar/r)**2d0+temp_mol_cloud**4d0)**0.25
      dzones(izone)=rho1; vrzones(izone)=vr1; vphizones(izone)=vphi1; vthetazones(izone)=vtheta1; tempzones(izone)=temp1
  
      if(loccell==2) then
        izone=2
        call calc_disk_flow(r,zcoord,rho1,temp1,vr1,vphi1,vtheta1,idisk)
        dzones(izone)=rho1; vrzones(izone)=vr1; vphizones(izone)=vphi1; vthetazones(izone)=vtheta1; tempzones(izone)=temp1
        if(dzones(1)>dzones(2)) loccell=1 ! If too low density, switch to the envelope
      end if
      if (loccell==2) routdisk=max(r,routdisk)
  
      rho=dzones(loccell); vr=vrzones(loccell); vphi=vphizones(loccell); vtheta=vthetazones(loccell); temp=tempzones(loccell)
  
      ! Limit velocity
      vx=vr*dsin(theta)+vtheta*dcos(theta)
      vz=vr*dcos(theta)-vtheta*dsin(theta)
      vx=vx*dexp(-brak_dist_x/xcoord)
      vz=vz*dexp(-brak_dist_z/zcoord)
      vr=vx*dsin(theta)+vz*dcos(theta)
      vtheta=vx*dcos(theta)-vz*dsin(theta)


      call get_extinction(r,theta,time,rho,loccell,Av)

    end if ! End if in outflow
    
  end if ! End if in star


  ! Use radmc temperature or not ?
  tadjust=0
  if (t_pstar_age) then
    tadjust=tff
  endif
  if(use_radmc_temp) then
    if (.not. particle .or. ((time-tadjust)>tminradmc .or. steadystate)) then
      if(r/au<rmaxradmc) then
       temp=interp_temp_radmc(time,r,theta)
      end if ! if r>rmaxradmc
    end if
  end if ! if particle


end subroutine




subroutine calc_BE_flow(time,r,rho,vr,vphi,vtheta)
  ! Calculate flow and density map from the Bonnor Ebert model
  use variables
  implicit none
  double precision :: rho,vr,vphi,vtheta
  double precision :: time,r
  integer :: ir


  vtheta= 0d0
  vphi  = 0d0


  if (particle) then
    call rt_to_rho_v(r,time,rho,vr,vphi)
  else
    if (loggrid) then
      ir = floor(dble(nrad-1)*dlog(r*(1d0+1d-10)/au/rad_min_au)/dlog(rad_max_au/rad_min_au))+1
    else
      ir = floor(1+(nrad-1d0)*(r*(1d0+1d-10)/au-rad_min_au)/(rad_max_au-rad_min_au))
    endif
    if (ir<1) then
      rho = 1d-20
      vr = 0d0
      vphi = 0d0
    else if (ir > nrad) then
      rho = 1d-20
      vr = vel_radii(nrad)
    else
      rho = densities_radii(ir)
      vr = vel_radii(ir)
      vphi = velphi_radii(ir)
    end if
  endif


end subroutine




subroutine calc_disk_flow(r,zcoord,rho,temp,vr,vphi,vtheta,idisk)
  use variables
  implicit none
  double precision, intent(in) :: r,zcoord
  integer, intent(in) :: idisk
  double precision :: rho,vr,vphi,vtheta,temp

  ! Get flow variables from pre-computed disk properties
  rho=densdisk(idisk)*dexp(-zcoord**2d0/(2d0*Hdisk(idisk)**2d0))
  vphi=dsqrt(gg*Mcen/r)
  vr=-3d0/2d0*alphadisk*csdisk(idisk)**2d0/vphi
  vtheta=0d0
  temp=tempdisk(idisk)

end subroutine



double precision function interp_temp_radmc(time,r,theta)
  use variables
  implicit none
  double precision, intent(in) :: time,r,theta
  integer :: iradmc,iradmcp
  integer :: iradius,iradiusp,itheta,ithetap
  double precision :: iradmc_r,iradius_r, itheta_r
  double precision :: temp_1_1,temp_2_1,temp_1_2,temp_2_2,temp_1,temp_2


  if(steadystate .or. .not. particle) then

    ! Find which location
    if (logradmc) then
      iradius_r=1d0+dble(nradradmc-1)*dlog(r/au/rminradmc)/dlog(rmaxradmc/rminradmc)
    else
      iradius_r=1d0+(nradradmc-1d0)*(r/au-rminradmc)/(rmaxradmc-rminradmc)
    endif
    itheta_r=(theta)/(pi/2d0)*dble(nthetaradmc)+1d0

    iradius_r=max(iradius_r,1d0)
    iradius_r=min(iradius_r,dble(nradradmc))
    itheta_r=max(itheta_r,1d0)
    itheta_r=min(itheta_r,dble(nthetaradmc-1))

    iradius=floor(iradius_r)
    iradiusp=min(iradius+1,nradradmc)
    itheta=floor(itheta_r)
    ithetap=min(itheta+1,nthetaradmc)

    ! Find temperatures to interpolate
    temp_1=(iradius_r-dble(iradius))*radmc_temperatures(1,iradiusp,itheta)+(dble(iradiusp)-iradius_r)*radmc_temperatures(1,iradius,itheta)
    temp_2=(iradius_r-dble(iradius))*radmc_temperatures(1,iradiusp,ithetap)+(dble(iradiusp)-iradius_r)*radmc_temperatures(1,iradius,ithetap)
    interp_temp_radmc=(itheta_r-dble(itheta))*temp_2+(dble(ithetap)-itheta_r)*temp_1
    interp_temp_radmc=max(interp_temp_radmc,temp_mol_cloud)

  else

    ! Find which files 
    iradmc_r=(time-tminradmc)/dtradmc+0.99d0
    if (t_pstar_age) then
      iradmc_r=(time-tff-tminradmc)/dtradmc+0.99d0
    endif
    iradmc=max(floor(iradmc_r),1)
    iradmcp=min(iradmc+1,nradmc)
    
    ! Find which location
    if (logradmc) then
      iradius_r=1d0+dble(nradradmc-1)*dlog(r/au/rminradmc)/dlog(rmaxradmc/rminradmc)
    else
      iradius_r=1d0+(nradradmc-1d0)*(r/au-rminradmc)/(rmaxradmc-rminradmc)
    endif
    itheta_r=(theta)/(pi/2d0)*dble(nthetaradmc)+1d0

    iradius_r=max(iradius_r,1d0)
    iradius_r=min(iradius_r,dble(nradradmc))
    itheta_r=max(itheta_r,1d0)
    itheta_r=min(itheta_r,dble(nthetaradmc-1))

    iradius=floor(iradius_r)
    iradiusp=min(iradius+1,nradradmc)
    itheta=floor(itheta_r)
    ithetap=min(itheta+1,nthetaradmc)

    ! Find temperatures to interpolate
    temp_1_1=(iradius_r-dble(iradius))*radmc_temperatures(iradmc,iradiusp,itheta)+(dble(iradius+1)-iradius_r)*radmc_temperatures(iradmc,iradius,itheta)
    temp_1_2=(iradius_r-dble(iradius))*radmc_temperatures(iradmc,iradiusp,ithetap)+(dble(iradius+1)-iradius_r)*radmc_temperatures(iradmc,iradius,ithetap)
    temp_1=(itheta_r-dble(itheta))*temp_1_2+(dble(itheta+1)-itheta_r)*temp_1_1

    temp_2_1=(iradius_r-dble(iradius))*radmc_temperatures(iradmcp,iradiusp,itheta)+(dble(iradius+1)-iradius_r)*radmc_temperatures(iradmcp,iradius,itheta)
    temp_2_2=(iradius_r-dble(iradius))*radmc_temperatures(iradmcp,iradiusp,ithetap)+(dble(iradius+1)-iradius_r)*radmc_temperatures(iradmcp,iradius,ithetap)
    temp_2=(itheta_r-dble(itheta))*temp_2_2+(dble(itheta+1)-itheta_r)*temp_2_1

    interp_temp_radmc=(iradmc_r-dble(iradmc))*temp_2+(dble(iradmc+1)-iradmc_r)*temp_1



  end if

end function


subroutine get_extinction(r,theta,time,rho,loccell,Av)
  use variables
  implicit none
  double precision,intent(in) :: r,time,rho,theta
  integer,intent(in) :: loccell
  double precision :: dum
  double precision :: r2,Av,rho2,coldens,zcoord
  double precision :: rmax
  double precision :: slope
  double precision :: r_vs_t
  integer :: idisk

  ! We interpolate density as a powerlaw : rho = fact*r**(slope)
  rmax=r_vs_t(xmax/scale_x,time)
  
  if (r>rmax .or. rho<=1.0d-19) then
    coldens=0d0
  else
    r2=(0.1*r+0.9*rmax)
    call rt_to_rho_v(r2,time,rho2,dum,dum)
    slope = dlog(rho2/rho)/dlog(r2/r)
    if(slope >= -1d0) then
      coldens=0d0
    else
      !fact = d/(r**slope)
      !coldens = fact/(slope+1d0)*(r2**(slope+1)-r**(slope+1d0))
      ! Trick to avoid machine precision issue
      coldens=dlog(rho)-slope*dlog(r)-dlog(-(slope+1d0))+dlog(1d0-(r2/r)**(slope+1d0))+(slope+1d0)*dlog(r)
      coldens=dexp(coldens)

      ! Adding disk contribution if necessary
      if (loccell == 2) then
        idisk=floor (r/rmaxdisk*nprofdisk+0.5)  
        zcoord=r*dcos(theta)
        coldens = coldens + dsqrt(pi/2d0) * Hdisk(idisk) * densdisk(idisk)*(1d0-erf(zcoord/(dsqrt(2d0)*Hdisk(idisk)))) ! Vertical integration on the disk
      end if

      coldens=coldens/mean/mp
    endif
  endif

  Av=coldens*AvFact+cloud_extinction


end subroutine



! Get index of the size-distribution in real
double precision function get_grain_index_real(rho,temp)
  use variables, ONLY:shark_dist
  implicit none
  double precision, intent(in) :: rho,temp
  double precision :: get_irhograin, get_ichi
  double precision :: get_chi,chi

  if (shark_dist) then
    get_grain_index_real=get_irhograin(rho)
  else
    chi=get_chi(rho,temp)
    get_grain_index_real=get_ichi(chi)
  end if

end function


! Get index of the size-distribution
integer function get_grain_index(rho,temp)
  use variables, ONLY:shark_dist
  implicit none
  double precision, intent(in) :: rho,temp
  double precision :: get_irhograin, get_ichi
  double precision :: get_chi,chi

  if (shark_dist) then
    get_grain_index=floor(get_irhograin(rho))
  else
    chi=get_chi(rho,temp)
    get_grain_index=floor(get_ichi(chi))
  end if

end function


! Get index of the size-distribution of Lebreuilly+23
double precision function get_irhograin(dens)
  use variables
  implicit none
  double precision, intent(in) :: dens
  integer :: i1,i2,imid

  if (coagulation) then
    if (dens<grain_rho(1)) then
      get_irhograin=1d0
    elseif (dens > grain_rho(nrhograins)) then
      get_irhograin=nrhograins+0d0
    else
      i1=1
      i2=nrhograins
      imid=nrhograins/2
      do while (i2-i1>1)
        if (dens > grain_rho(imid)) then
          i1=imid
        else
          i2=imid
        end if
        imid=int((i1+i2)/2)
      end do
      get_irhograin=imid+(dens-grain_rho(imid))/(grain_rho(imid+1)-grain_rho(imid))
    end if
  else
    get_irhograin=1
  endif

end function


double precision function get_chi(rho,temp)
  ! Calculate chi with prescription
  use variables, ONLY: dust_to_gas
  implicit none
  double precision, intent(in) :: rho,temp
  if(rho<1d-15) then
    get_chi=1d16*(rho/3d-20)**0.25
  else
    get_chi=1.34d18*(rho/3.83d-12)**(1d0/15d0)*(temp/1d1)**(1d0/15d0)
  endif
  get_chi=get_chi*dust_to_gas/1d-2
end function

double precision function get_ichi(chi)
  ! Get index of the size-distribution of Marchand+23
  use variables, only:nchi,chimin,chimax,coagulation
  implicit none
  double precision, intent(in) :: chi
  if(coagulation) then
    get_ichi=1+dble(nchi-1)*log(chi/chimin)/log(chimax/chimin)
    get_ichi=max(get_ichi,1d0)
    get_ichi=min(get_ichi,nchi+0d0)
  else
    get_ichi=1d0
  end if
end function

double precision function get_outflow_density(zcoord)
  ! We use the prescription : dens = dens0 * (z/z0)^-2
  use variables,only: dens_outf_ref,height_outf_ref,mean,mp
  implicit none
  double precision, intent(in) :: zcoord
  get_outflow_density=dens_outf_ref*(height_outf_ref/zcoord)**2d0*mean*mp
end function

