! Particle evolution through time
subroutine evol_particle(tsys)
  use variables
  implicit none
  double precision, intent(in) :: tsys
  integer :: idt,loccell,i
  integer :: iter
  double precision :: time,xcoord,zcoord,r,theta,rho,vx,vz,vr,vtheta,vphi,temp,tpart,Av
  double precision :: time_old, rho_new, temp_new, Av_new
  double precision :: time_interp, rho_interp, temp_interp, Av_interp
  double precision :: norbits
  double precision :: accreted_mass
  double precision :: chi_dust
  double precision :: interpolate
  integer :: get_grain_index
  integer :: npart,ipart
  character(5) :: numpartchar
  integer :: ipartstart,ipartdone
  character(3) :: locpart
  character(20) :: dirname

  
  if(grid_of_part) then
    npart=ncell
    if (myid==0) then
      do ipart=1,npart
        xcoord=xcell(ipart,1)
        zcoord=xcell(ipart,2)
        open(103,file="particles/grid_of_part.dat",access="append")
        write(103,*) ipart, xcoord/au, zcoord/au
        close(103)
      end do
    end if
  else
    npart=1
  end if

  ipartstart=myid+1
  ipartdone=0
  ! Loop on all particles
  do ipart=ipartstart,npart,nproc
    ipartdone=ipartdone+1

    if(nproc > 1) write(*,"(A4,I2,3X,A5,I6,A15,I6,A1,I6)") "CPU=", myid,"Part=", ipart, " ||  Progress =",ipartdone,"/",npart/nproc
    tpart=0d0
    if(reverse) tpart=tsys   ! If going back in time

    ! Attribute initial position
    if(grid_of_part) then
      xcoord=xcell(ipart,1)
      zcoord=xcell(ipart,2)
    else
      xcoord=x_ini
      zcoord=z_ini
    end if
      
    idt=0
    norbits=0d0
    temp=temp_mol_cloud
    chi_dust=0d0
  
    if(steadystate) then
      Mcen=accreted_mass(tsys)
      call calc_disk_properties(tsys,1d0,1d0)
    end if


    ! Initializing files
    if(grid_of_part) then
      write(numpartchar,"(I5)") ipart
      if(ipart<10) then
        numpartchar="0000"//trim(adjustl(numpartchar))
      elseif(ipart<100) then
        numpartchar="000"//trim(adjustl(numpartchar))
      elseif(ipart<1000) then
        numpartchar="00"//trim(adjustl(numpartchar))
      elseif(ipart<10000) then
        numpartchar="0"//trim(adjustl(numpartchar))
      end if
      dirname="particles/part_"//trim(adjustl(numpartchar))
      call execute_command_line("mkdir "//dirname)
      open(102,file=trim(adjustl(dirname))//"/structure_evolution.dat",status="replace")
    else
      open(102,file="structure_evolution.dat",status="replace")
    end if
    write(102,*) "! time    log(Av)     log(nH)      log(T)"
    write(102,*) "! (yr)    log(mag)    log(cm-3)   log(K)"
  

    iter=0

    ! Start main time loop
    do while((.not. reverse .and. tpart<tmax .and. idt<ndt_max) .or. (reverse .and. tpart>0))
      iter=iter+1
  
      ! System properties
      if(.not. steadystate) then
        time=tpart
        Mcen=accreted_mass(time)
        if (time>tff) then
          star=.true.
          disk=.true.
          call calc_disk_properties(tpart,xcoord,zcoord)
        else
          star=.false.
          disk=.false.
          Lstar=0d0
          Rstar=0d0
          Mstar=0d0
          Tstar=0d0
          Mdisk=0d0
          Rdisk=0d0
        end if
      else
        time=tsys
      end if
  
      
      ! Density velocity temperature calculations
      call calc_flow(time,xcoord,zcoord,rho_new,temp_new,vr,vphi,vtheta,loccell,Av_new)

      if (iter > 1 .and. smooth .and. (abs(log10(rho_new/rho)) >= log10(jump_factor) .or. abs(log10(temp_new/temp)) >= log10(jump_factor))) then
        do i=1,nsteps_jump
          time_interp = time_old + (time-time_old)*dble(i)/dble(nsteps_jump+1)
          rho_interp=interpolate(time_old,time,dlog10(rho*2/(mean*mp)),dlog10(rho_new*2/(mean*mp)),time_interp)
          temp_interp=interpolate(time_old,time,dlog10(temp),dlog10(temp_new),time_interp)
          Av_interp=interpolate(time_old,time,dlog10(Av),dlog10(Av_new),time_interp)
          write(102,format_nautilus) time_interp/s2year, Av_interp, rho_interp, temp_interp
        end do
      end if

      rho=rho_new
      temp=temp_new
      Av=Av_new
      time_old=time



      ! Console output
      locpart="env"
      if(loccell==2) locpart="dsk"
      if(npart==1) write(*,"(A14,X,f7.0,A10,X,f9.3,X,A7,X,f9.3,2X,A8,X,A3,2X,A2,X,f7.4,A14,X,f6.2,A13,f7.1,A4,f6.1,A2)") "Particle at t=",tpart/s2year,"years,  x=",xcoord/au,"au,  z=",zcoord/au, "au,  loc", locpart, "M=",Mcen/Msun, " Msun,  Rdisk=", rdisk/au, " au,  Norbits= ", norbits, "  T=",temp," K"
  
      if(loccell==3 .or. sqrt(xcoord**2d0+zcoord**2d0)<2d0*au) then
        print*, ""
        print*, "Particle", ipart, " in star !"
        print*, ""
        if (reverse) then
          write(102,format_nautilus) 0d0, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
          write(102,format_nautilus) 0d0, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
        else
          write(102,format_nautilus) tpart/s2year, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
        end if
        exit
      end if
      if(loccell==4) then
        print*, ""
        print*, "Particle", ipart, " in outflow !"
        print*, ""
        if (reverse) then
          write(102,format_nautilus) 0d0, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
          write(102,format_nautilus) 0d0, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
        else
          write(102,format_nautilus) tpart/s2year, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
        end if
        exit
      end if



      r=dsqrt(xcoord**2d0+zcoord**2d0)
      theta=datan(xcoord/zcoord)
      vx=vr*dsin(theta)+vtheta*dcos(theta)
      vz=vr*dcos(theta)-vtheta*dsin(theta)

      ! Write data 
      write(102,format_nautilus) tpart/s2year, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
      if (iter==1 .and. reverse) write(102,format_nautilus) tpart/s2year, log10(Av), log10(rho*2/(mean*mp)), log10(temp) ! We duplicate the last time-step
      if (shark_dist) then
        if (.not. grid_of_part) write(101,"(8(e15.7,2X),I6,8X,I1,6(2X,e15.7))") tpart/s2year,xcoord/au,zcoord/au,rho,temp,vr,vphi,vtheta,get_grain_index(rho),loccell, Mstar/msun, Rstar/rsun, Lstar/lsun, Tstar, Mdisk/msun, Rdisk/au
      else
        if (.not. grid_of_part) write(101,"(9(e15.7,2X),6X,I1,6(2X,e15.7))")    tpart/s2year,xcoord/au,zcoord/au,rho,temp,vr,vphi,vtheta,chi_dust            ,loccell, Mstar/msun, Rstar/rsun, Lstar/lsun, Tstar, Mdisk/msun, Rdisk/au
      endif
  
      ! New time-step
      dt=dt0
      if(adaptive_dt .and. abs(vr)>0d0) dt=min(abs(r/vr)/dyn_fact,dt0)
      if(reverse) dt=-min(dt,tpart)
      if(reverse .and. tpart>tff .and. tpart+dt<tff .or. .not. reverse .and. tpart<tff .and. tpart+dt>tff) then
        dt=tff-tpart
      end if
      xcoord=xcoord+dt*vx
      zcoord=zcoord+dt*vz
      tpart=tpart+dt
      idt=idt+1
      norbits=norbits+vphi*dabs(dt)/(2d0*pi*r)
  

      if(reverse .and. tpart<=0d0) then
        print*, ""
        print*, "Particle ",ipart," reached t=0 !"
        write(102,format_nautilus) 0d0, log10(Av), log10(rho*2/(mean*mp)), log10(temp)
        print*, ""
        exit
      end if
      if(idt==ndt_max) then
        print*, ""
        print*, "Particle ",ipart," reached max number of iterations !"
        print*, ""
        exit
      end if
      if(tpart>=tmax) then
        print*, ""
        print*, "Particle ",ipart," reached maximum time !"
        print*, ""
        exit
      end if
  
  
    end do ! End main time loop
    close(102)

    

  end do ! End loop on particles

end subroutine



double precision function interpolate(x1,x2,y1,y2,x)
  implicit none
  double precision, intent(in) :: x1,x2,y1,y2,x
  interpolate = ((x-x1)*y2 + (x2-x)*y1)/(x2-x1)
end function


