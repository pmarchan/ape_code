MODULE variables
  ! Variables declaration
  implicit none

  ! Constants
  double precision,parameter :: mp=1.67e-24        ! Proton mass
  double precision,parameter :: me=9.109d-28       ! Electron mass
  double precision,parameter :: kb=1.38e-16        ! Botlzmann constant
  double precision,parameter :: pi=3.14159265359d0 ! pi
  double precision,parameter :: gg=6.67e-8         ! Gravitational constant
  double precision,parameter :: e=4.8d-10          ! Electric charge
  double precision,parameter :: c=3d10             ! Light speed
  double precision,parameter :: gamma=1.66667      ! Adiabatic index
  double precision,parameter :: s2year=86400d0*365.25d0 ! Seconds in a year
  double precision,parameter :: au=1.5d13          ! Au in cm
  double precision,parameter :: msun=2d33          ! Solar mass
  double precision,parameter :: rsun=6.96d10       ! Solar radius
  double precision,parameter :: lsun=3.839d33      ! Solar luminosity
  double precision,parameter :: sigmab=5.67d-5     ! Stefan constant
  double precision,parameter :: mean=2.3           ! Mean gas atomic weight
  double precision,dimension(7) :: T_opac_regimes  ! Opacity regimes min and max temperatures
  double precision,dimension(7,2) :: Opac_coeff  ! Opacity coefficients (kappa0, n)
  double precision,parameter :: AvFact=5.34d-22  ! Conversion factor Av-NH2
  double precision,parameter :: eta_D=1d18       ! Resistivity (Ohm+ambipolar) for disk size



  ! Grid parameters
  logical :: loggrid=.false.          ! log spacing for the grid
  integer :: ntheta = 100             ! Number of cells in theta direction
  integer :: nrad = 100               ! Number of cells in radial direction
  integer :: nmaxcell = 100000        ! Number of cells max
  integer :: ncell = 100000           ! Number of cells 
  double precision :: rad_min_au = 1d0     ! Minimum radius
  double precision :: rad_max_au = 1d0     ! Maximum radius
  double precision :: theta_min_deg = 0d0      ! Minimum angle
  double precision :: theta_max_deg = 90d0     ! Maximum angle
  double precision :: theta_min,theta_max      ! Angles in radians
  logical :: switchresol=.false.      ! Switch resol because dx too high
  double precision,dimension(:,:), allocatable :: xcell         ! Cell coordinates in cartesian
  double precision,dimension(:,:), allocatable :: rtcell        ! Cell coordinates in spherical
  double precision,dimension(:), allocatable :: sizecell        ! Cell size
  double precision,dimension(:), allocatable :: radii           ! Radial sampling of space
  double precision,dimension(:), allocatable :: densities_radii ! Radial sampling of density for envelope
  double precision,dimension(:), allocatable :: vel_radii       ! Radial sampling of velocity for envelope
  double precision,dimension(:), allocatable :: velphi_radii    ! Radial sampling of azimuthal velocity for envelope

  ! Bonnor-Ebert parameters
  integer, parameter :: nsampx = 100000                     ! Sampling in x for BE
  double precision, parameter :: xmax=6.451                 ! Max x, you can change it to extend the envelope further
  double precision, parameter :: xmax_ref=6.451             ! Max reference x, used to determine the central density and the free-fall time
  double precision, parameter :: dx=xmax/nsampx             ! Sampling width
  double precision, dimension(nsampx) :: x_be               ! Sampling x BE
  double precision, dimension(nsampx) :: rho_be             ! Sampling density BE
  double precision, dimension(nsampx) :: phi_be             ! Sampling gravitational potential BE
  double precision, dimension(nsampx) :: mass_be            ! Sampling mass BE
  double precision :: scale_x                               ! Scaling factor between x and r
  double precision :: rhoc                                  ! Initial central density


  ! Flow variables
  double precision,dimension(:), allocatable :: rhocell   ! Cell density
  double precision,dimension(:), allocatable :: tcell     ! Cell temperature
  double precision,dimension(:), allocatable :: temp_radmc ! Cell temperature after radmc-3d
  double precision,dimension(:), allocatable :: Avcell    ! Estimation of visual extinction (for post-processing chemistry)
  integer,dimension(:), allocatable :: igraincell         ! Index for coagulation
  integer, dimension(:), allocatable :: wherecell         ! Sound-speed of the disk mid-plane
  integer,dimension(:), allocatable :: loc                ! Cell localization ! 1=envelope, 2=disk, 3=star, 4=outflow
  double precision,dimension(:,:), allocatable :: vel     ! Cell velocity: vr,vtheta,vx,vz
  double precision :: Rc
  double precision :: Mcen=0d0           ! Accreted mass
  double precision :: Bdisk=0d0          ! Disk's magnetic field
  logical :: star=.false.                ! Is there a protostar yet
  logical :: disk=.false.                ! Is there a disk yet
  double precision :: rdisk=0d0          ! Disk radius
  double precision :: rlimdisk=0d0       ! Limit of disk radius
  double precision :: routdisk=0d0       ! Outer disk radius
  double precision :: rdisk_in=0d0       ! Inner disk radius
  double precision :: Mdisk=0d0          ! Disk mass
  double precision :: Mstar=0d0          ! Star mass
  double precision :: Lstar=0d0          ! Star Luminosity
  double precision :: Rstar=0d0          ! Star radius
  double precision :: Tstar=0d0          ! Star temperature
  double precision :: time_star=0d0      ! Protostar formation time
  double precision :: mdot=0d0           ! Accretion rate

  ! Model parameters
  double precision :: omega0=1d-13           ! Initial angular velocity
  double precision :: Mass=1d0               ! Mass of the initial dense core
  double precision :: tff                    ! Initial free-fall time
  double precision :: rhostar=1d-8           ! Density of protostar creation
  double precision :: rhodisk=1d-13          ! Density of disk creation
  double precision :: masstoflux=5           ! Mass to flux ratio
  double precision :: alphadisk=1d-2         ! Parameter alpha (Shakura and Sunyaev)
  double precision :: time_years=0d0         ! Time for the snapshot
  logical :: t_pstar_age=.false.             ! Is time_years the age of the protostar ?
  double precision :: Tevap=1.7d3            ! Evaporation temperature for grains
  double precision :: temp_mol_cloud   ! Molecular cloud temperature
  double precision :: cs                     ! Sound speed
  double precision :: tacc                   ! Accretion time for outflow cavity
  double precision :: dens_outf_ref=1d4      ! Reference number density of gas in outflow cavity
  double precision :: height_outf_ref=1d3*au ! Reference height for density in outflow cavity
  double precision :: dens_cutoff=2d0        ! Density cutoff at center in au, decrease in exp(-(dens_cutoff/r)**2.0)
  double precision :: cloud_extinction=4.5 ! Contribution to extinction from the surrounding cloud
  logical :: disk_cutoff = .true.         ! Sharply cut the disk at r=rdisk
  double precision :: AspectRatioMax=99  ! Maximum value of the disk aspect ratio.

  ! Coagulation parameters
  logical :: coagulation=.true.                                 ! Estimate of coagulation ?
  double precision,dimension(:),allocatable :: tableline        ! To read coagulation table
  double precision,dimension(:),allocatable :: grain_size       ! Size of grains for Lebreuilly+23
  double precision,dimension(:,:),allocatable :: grain_size_chi ! Size of grains for Marchand+23
  double precision,dimension(:),allocatable :: grain_rho        ! Density sample of grain size distribution
  double precision,dimension(:),allocatable :: grain_mass       ! Mass of grains
  double precision,dimension(:,:),allocatable :: grain_abun     ! Normalized number density of grains
  double precision,dimension(:,:),allocatable :: grain_dens     ! Normalized mass density of grains
  double precision, parameter :: rhograin=2.3                 ! Bulk density of grains
  double precision :: dust_to_gas = 1d-2                      ! Dust-to-gas mass ratio
  integer, parameter :: nrhograins=128                        ! Number of density points for grain size-distribution of Lebreuilly+23
  integer :: nchi                                             ! Number of density points for grain size-distribution of Marchand+23
  integer :: ngrainpoints=128                                 ! Number of density points for grain size-distribution of Lebreuilly+23
  integer :: nsize=20                                         ! Number of grain size bins
  integer :: nopac                                            ! Number of density points for opacities
  double precision :: chimin, chimax                          ! chi min and max for the table of Marchand+23
  logical :: shark_dist=.true.                                ! Use coagulation model of Lebreuilly+23 (.true.) or Marchand+23 (.false.)

  ! Particle mode
  logical :: particle=.false.                   ! Activate particle mode
  double precision :: tmax=1d6                  ! Maximum integration time in years
  double precision :: dt                        ! Time-step in years
  double precision :: dt0=2d2                 ! Time-step in years
  double precision :: x_ini=1000d0               ! Initial coordinates in au
  double precision :: z_ini=1000d0               ! Initial coordinates in au
  character(127)   :: part_file="part_traj.dat" ! particle dump file
  logical :: adaptive_dt=.true.                 ! Adapt dt
  integer :: ndt_max=100000                     ! Max dt for particles
  double precision :: brak_dist_x=1d-1*au/1d1   ! Braking distance for particles in x direction
  double precision :: brak_dist_z=2d0*au/1d1    ! Braking distance for particles in y direction
  logical :: steadystate=.false.                ! Have a steady-state system 
  logical :: reverse=.false.                    ! Reverse the flow of time
  logical :: grid_of_part=.false.               ! Track the evolution of a grid of particles
  double precision :: dyn_fact=1d1              ! Factor for dt limitation in particle mode
  double precision :: jump_factor=1d2           ! Density and temperature threshold to detect a jump
  integer :: nsteps_jump=10                      ! Density and temperature threshold to detect a jump
  logical :: smooth=.true.                      ! Activate smoothing of density/temperature jump

  ! For the disk profile calculation
  integer, parameter :: nprofdisk=5000   ! Number of points for the disk profile
  double precision :: rmaxdisk=500d0*au  ! Max disk radius
  double precision, dimension(nprofdisk) :: raddisk      ! Radial coordinates for the profile
  double precision, dimension(nprofdisk) :: sigmadisk    ! Surface density of the disk
  double precision, dimension(nprofdisk) :: densdisk     ! Density of the disk mid plane
  double precision, dimension(nprofdisk) :: tempdisk     ! Temperature of the disk
  double precision, dimension(nprofdisk) :: Hdisk        ! Scale height of the disk
  double precision, dimension(nprofdisk) :: csdisk       ! Sound-speed of the disk mid-plane
  integer,parameter :: ntemp_opac=996                    ! For opacity tabulation 
  double precision :: tmin_opac=10d0                     ! Min tabulated temperature
  double precision :: tmax_opac=2000d0                   ! Max tabulated temperature
  double precision, dimension(:,:),allocatable :: opac_table  ! Tabulated opacities
  double precision :: sigma0                             ! Reference surface density of the disk
  double precision, parameter :: TC1=750                 ! Starting evaporation temperature of C-grains
  double precision, parameter :: TC2=1100                ! Ending evaporation temperature of C-grains
  double precision, parameter :: TSi1=1200               ! Starting evaporation temperature of Si-grains
  double precision, parameter :: TSi2=1300               ! Ending evaporation temperature of Si-grains
  double precision, parameter :: TAl1=1600               ! Starting evaporation temperature of Al-grains
  double precision, parameter :: TAl2=1700               ! Ending evaporation temperature of Al-grains
  double precision, parameter :: C_fraction=0.883        ! Fraction of C grains
  double precision, parameter :: Si_fraction=0.112       ! Fraction of Si grains
  double precision, parameter :: Al_fraction=0.005       ! Fraction of Al grains
  
 ! For the protostar calculation
 integer, parameter :: nmass_HO09 = 200                       ! Number of sampling point of HO09 for protostar mass.   m to i = 1+(nmass-1)*dlog(m/min_mstar)/dlog(max_mstar/min_mstar)
 integer, parameter :: nrad_HO09 = 200                       ! Number of sampling point of HO09 for protostar radius.
 integer, parameter :: nmdot_HO09 = 5                       ! Number of sampling point of HO09 for protostar accretion rate. 
 double precision :: min_mstar, max_mstar                ! Resampling min and max mass 
 double precision :: min_rstar, max_rstar                ! Resampling min and max radii 
 double precision, dimension(nmdot_HO09,nmass_HO09,3) :: HO09_track_resampled   ! HO09 resampled regularly
 double precision, dimension(nmass_HO09,nrad_HO09) :: HO09_mdot_rm   ! HO09 resampled regularly
 double precision, dimension(nmdot_HO09) :: HO09_mdot             ! HO09 mdot values
 double precision :: eta_disk = 0.75                     ! Free parameter for relative mass of disk/star
 double precision :: eta_star = 0.5                      ! Free parameter for the star luminosity
 double precision :: Md                                  ! Coefficient for star/disk mass ratio
 double precision :: ustar                               ! Coefficient for star/disk mass ratio

 ! Override variables
 logical :: override=.false.                             ! Override algorithm and impose values
 double precision :: mdisk_over=0d0                      ! Disk mass override
 double precision :: rdisk_over=0d0                      ! Disk radius override
 double precision :: rdisk_in_over=0d0                   ! Inner disk mass override
 double precision :: mstar_over=0d0                      ! Star mass override
 double precision :: rstar_over=0d0                      ! Star radius override
 double precision :: tstar_over=0d0                      ! Star temperature override
 double precision :: lstar_over=0d0                      ! Star luminosity override
 double precision :: outflow_angle_over=0d0              ! Outflow angle opening override

 ! For reading temperature from radmc3d
 double precision,dimension(:,:,:),allocatable :: radmc_temperatures  ! Temperatures from radmc3d
 double precision :: tminradmc                           ! Time of the first snapshot
 double precision :: tmaxradmc                           ! Time of the last snapshot
 integer  :: nradmc                                      ! Number of snapshots
 double precision :: dtradmc                             ! dt between each snapshot
 double precision :: rmaxradmc                           ! Size of the box in radmc snapshot
 double precision :: rminradmc                           ! Min radius in radmc snapshot
 integer :: nradradmc                                    ! Resolution in radius
 integer :: nthetaradmc                                  ! Resolution in angle
 logical :: logradmc                                     ! Is radmc in log scale in radius
 logical :: use_radmc_temp=.false.                       ! Activate temperature interpolation of radmc results
 double precision :: drradmc                             ! Radial resolution in radmc snapshot
 character(127) :: radmc_dir="temp_radmc/"               ! Directory in which radmc3d temperature are stored

 ! Outputs
 logical :: radmc_output=.false.                          ! Add radmc3d input for output
 logical :: snapshot_nautilus=.false.              ! Add grid_of_part style outputs to run nautilus on a snapshot
 double precision :: nautilus_time_kyr   ! Time for the snapshot nautilus simulation
 character(12) :: format_nautilus='(4(e16.6,X))'

 ! MPI
 integer :: ierror
 integer :: nproc
 integer :: myid

 ! Other
 character(len=255) :: pathape
 character(len=255) :: pathlib
 character(len=255) :: pathscripts

end MODULE
