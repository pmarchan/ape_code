subroutine initialize_be()
  ! Called at the begining to get the scaling parameters, the free-fall time etc.
  use variables
  implicit none
  integer :: i, i_ref
  integer :: xtoi


  cs=dsqrt(kb*temp_mol_cloud/(mp*mean))


  do i=1,nsampx
    x_be(i)=i*dx
  end do
  ! Integration of the Lane-Emden equation to get the BE profile
  phi_be(1)=0d0
  phi_be(2)=0d0
  rho_be(1)=1d0
  rho_be(2)=1d0
  mass_be(1)=0d0
  mass_be(2)=1d0*x_be(1)**2d0*dx
  do i=2,nsampx-1
    phi_be(i+1)=(dx**2d0*dexp(-phi_be(i))+2d0*phi_be(i)*(1d0+dx/x_be(i))-phi_be(i-1))/(1d0+2d0*dx/x_be(i))
    rho_be(i+1)=dexp(-phi_be(i+1))
    mass_be(i+1)=mass_be(i)+rho_be(i)*x_be(i)**2d0*dx
  end do

  ! Central density and everything that depends on it
  i_ref = xtoi(xmax_ref)
  rhoc = mass_be(i_ref)**2d0*cs**6d0*gg**(-3d0)*(4d0*pi)**(-1d0)*(Mass*msun)**(-2d0)
  scale_x = dsqrt(4d0*pi*gg*rhoc)/cs
  tff = dsqrt(3d0*pi/(32d0*gg*rhoc))

end subroutine

integer function xtoi(x)
  ! From x to index
  use variables, only:dx,xmax
  implicit none
  double precision,intent(in) :: x
  if (x > xmax) then
    print*, "ERROR, going to far away"
  else
    xtoi=floor(x/dx)
  end if
end function


double precision function mass_inside(r0)
  ! Mass within r0 at t=0
  use variables
  implicit none
  double precision, intent(in) :: r0
  integer :: iofx
  integer :: xtoi
  double precision :: x1,x2,m1,m2,x0
  x0=r0*scale_x
  iofx=xtoi(x0)
  ! We interpolate to avoid discontinuities
  if (iofx==nsampx) then
    mass_inside = Mass*msun
  else
    x1 = x_be(iofx)
    x2 = x_be(iofx+1)
    m1 = mass_be(iofx)
    m2 = mass_be(iofx+1)
    mass_inside = (m2*(x0-x1)+m1*(x2-x0))/(x2-x1)
    mass_inside = mass_inside*cs**3d0*gg**(-1.5d0)*(4d0*pi*rhoc)**(-0.5d0)
  endif
end function


double precision function r0_to_rho(r0)
  ! Density at radius r0 at t=0
  use variables, only:rhoc,scale_x,rho_be,x_be,nsampx
  implicit none
  double precision, intent(in) :: r0
  integer :: iofx
  integer :: xtoi
  double precision :: x1,x2,rho1,rho2,x0
  x0=r0*scale_x
  iofx=xtoi(x0)
  ! We interpolate to avoid discontinuities
  if (iofx==nsampx) then
    r0_to_rho=rho_be(nsampx)
  else
    x1 = x_be(iofx)
    x2 = x_be(iofx+1)
    rho1 = rho_be(iofx)
    rho2 = rho_be(iofx+1)
    r0_to_rho = (rho2*(x0-x1)+rho1*(x2-x0))/(x2-x1)*rhoc
  endif
end function


double precision function r_vs_t(r0,time)
  ! Compute radius of shell at time t starting at r=r0
  use variables, only:gg
  implicit none
  double precision, intent(in) :: r0,time
  double precision :: m0
  double precision :: mass_inside
  m0=mass_inside(r0)
  r_vs_t = r0**1.5d0 - 1.5d0*dsqrt(gg*m0/2d0)*time
  if (r_vs_t <= 0d0) then
    r_vs_t=0d0
  else
    r_vs_t=r_vs_t**(2d0/3d0)
  end if
end function


double precision function eq_r0(r,time,r0)
  ! Equation to solve to go find r0
  implicit none
  double precision, intent(in) :: r,time,r0
  double precision :: r_vs_t   
  double precision :: rproj
  rproj = r_vs_t(r0,time)
  eq_r0 = (rproj-r)/r0
end function


double precision function r_to_r0(r,time)
  ! Find r0 from r and t by dichotomy
  use variables, only:dx,xmax,scale_x
  implicit none
  double precision, intent(in) :: r,time
  double precision :: eps_conv = 1d-14
  double precision :: rad_l, rad_u, rad_m
  double precision :: eq_r0
  rad_l = dx/scale_x*1.001d0
  rad_u = xmax/scale_x*0.999d0
  rad_m = dsqrt(rad_l*rad_u)
  do while (abs(rad_u-rad_l)/rad_m > eps_conv)
    if (eq_r0(r,time,rad_l)*eq_r0(r,time,rad_m) > 0d0) then
      rad_l = rad_m
    else
      rad_u = rad_m
    end if
    rad_m = dsqrt(rad_l*rad_u)
  end do
  r_to_r0=rad_m
end function


double precision function accreted_mass(time)
  ! Compute accreted mass within 1 au, to be distributed between the protostar and disk
  use variables, only: dx, scale_x
  implicit none
  double precision, intent(in) :: time
  double precision :: rini
  double precision :: mass_inside
  double precision :: r_to_r0
  rini = r_to_r0(dx/scale_x*1.002d0,time)
  accreted_mass = mass_inside(rini)
end function
  

subroutine rt_to_rho_v(r,time,rho,vr,vphi)
  ! Compute density at time t and radius r
  ! By calculating the volume compression factor
  use variables, only: gg,Mass, xmax, scale_x, omega0, au
  implicit none
  double precision, intent(in) :: r,time
  double precision :: rho,vr,vphi
  double precision :: r1,r2,volume,volume_0,rho0,r0_1,r0_2
  double precision :: r_to_r0,r0_to_rho,m0, mass_inside
  double precision :: rad_max
  rad_max = xmax/scale_x*0.999d0
  r1=r
  r2=r*1.01d0
  r0_1=r_to_r0(r1,time)
  if (abs(r0_1-rad_max)/rad_max < 1d-6) then
    rho=1d-20
    vr=-dsqrt(gg*Mass/(2d0*rad_max))
  else
    r0_2=r_to_r0(r2,time)
    rho0=r0_to_rho(r0_1)
    volume = (r2**3d0-r1**3d0)
    volume_0 = (r0_2**3d0-r0_1**3d0)
    rho=rho0*volume_0/volume
    m0=mass_inside(r0_1)
    vr=-dsqrt(gg*m0/(2d0*r))
  end if
  vphi=omega0*r0_1**2d0/r
end subroutine



subroutine compute_envelope_density(time)
  use variables, only: nrad, radii, densities_radii, vel_radii, velphi_radii
  implicit none
  double precision, intent(in) :: time
  double precision :: rho,vr,vphi
  integer :: i
  densities_radii = 0d0
  vel_radii = 0d0
  velphi_radii = 0d0
  do i=1,nrad
    call rt_to_rho_v(radii(i),time,rho,vr,vphi)
    densities_radii(i) = rho
    vel_radii(i) = vr
    velphi_radii(i) = vphi
  end do
end subroutine


double precision function acc_rate(time)
  implicit none
  double precision, intent(in) :: time
  double precision :: accreted_mass
  acc_rate=(accreted_mass(time*1.01d0)-accreted_mass(time*0.99d0))/(0.02d0*time)
end function
