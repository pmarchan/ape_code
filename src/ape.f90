program ape
  use variables
  implicit none
#if PARAL==1
  include 'mpif.h'
#endif
  character(127) :: paramfile
  double precision :: t
  integer :: pathlen
  
#ifdef CURRENT_DIR
    pathape = CURRENT_DIR
#else
    pathape = "Path not provided"
#endif

  pathlen=len(trim(adjustl(pathape)))
  pathape=pathape(1:pathlen-3)
  pathlib=trim(adjustl(pathape))//"/lib/"
  pathscripts=trim(adjustl(pathape))//"/scripts/"


  ! Read parameter file and initialize
  call getarg(1,paramfile)
  call read_params(paramfile)
  call initialize()

  ! MPI stuff if needed
#if PARAL==1
  call MPI_INIT(ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, nproc, ierror)
  call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierror)
#else
  nproc=1
#endif

  if(myid==0) then
    print*,""
    print*,""
    print*, " ----------------------------------------"
    print*, " Welcome to the APE code !"
    print*, " Analytical Protostellar Environment"
    print*, " Credit : Pierre Marchand, 2024"
    print*, ""
    print*, "                           _            " 
    print*, "                          |_\           " 
    print*, "                ______    |_|\          " 
    print*, "               /      \     \ \         " 
    print*, "             @|  0  0  |@    \ \        " 
    print*, "              |        |     / /        " 
    print*, "               \   O  /     / /         " 
    print*, "           _____\    /_____/ /          "  
    print*, "          / _____    _______/           " 
    print*, "         / /    |    |   ____           " 
    print*, "        / /_/\_ |    |  //  \\          " 
    print*, "        \_____/ |  . | //  _ \\         " 
    print*, "                /    \//  //  \\        " 
    print*, "               / ____ \   \\  //        " 
    print*, "              / /    \ \   \\//         " 
    print*, "              | |    | |    \/          " 
    print*, "              / /    \ \                " 
    print*, "             /  \    /  \               " 
    print*, "             |_|_|   |_|_|              " 
    print*, ""
    print*,""
    print*,""
    print*,""
  end if


  if(particle) then
    if(myid==0) print*, "PARTICLE MODE"
    if(myid==0 .and. grid_of_part) then
      call execute_command_line("mkdir particles")
      open(103,file="particles/grid_of_part.dat",status="replace")
      close(103)
    endif

#if PARAL==1
    call MPI_BARRIER(MPI_COMM_WORLD,ierror)
#endif
    call evol_particle(time_years*s2year)
#if PARAL==1
    call MPI_BARRIER(MPI_COMM_WORLD,ierror)
#endif

    if(reverse) then
      if(grid_of_part) then
#if PARAL==1
        if(myid==0) then
#endif
          call execute_command_line("sh "//trim(adjustl(pathscripts))//"/reverse_particles.sh 1")
#if PARAL==1
        end if
#endif
      else
        call execute_command_line("sh "//trim(adjustl(pathscripts))//"/reverse_particles.sh 0")
      endif
    end if

  else
    ! Snapshot mode
    if(myid==0) then
      t=time_years*s2year
      print*, "SNAPSHOT MODE"
      print*, "Resolution", ncell, "cells"
      print*, ""
      call snapshot(t)
    end if
  end if

if (myid==0) then
  print*, ""
  print*, ""
  print*, ""
  print*, ""
end if

#if PARAL==1
  call MPI_FINALIZE(ierror)
#endif
end program
