subroutine calc_star_properties(time)
  use variables
  implicit none
  double precision, intent(in) :: time
  double precision :: radfrommassmdot, massfromrad,lumfrommassmdot
  double precision :: acc_rate, mdot_total, mdot_star


  if(star) then

    ! Estimate of Mstar and Rstar
    Mstar=Mcen*0.75
    mdot_total=acc_rate(time)
    !if (.not. particle) then
      !print*, "Mdot = ", mdot_total*s2year/msun, "Msun / year"
    !end if
    mdot_star=mdot_total*0.75
    if (mdot_total < HO09_mdot(1)) then
      mdot_star=HO09_mdot(1)
    end if
    Rstar=radfrommassmdot(Mstar,mdot_star)

    ! Calculate disk radius consistent with mass
    call get_radius(Rstar,mdot_star)
    Mstar=massfromrad(Rstar)
    Lstar=lumfrommassmdot(Mstar,mdot_star)
    if(override .and. mstar_over/=0d0) Mstar=Mstar_over*msun
    if(override .and. mstar_over/=0d0) Rstar=Rstar_over*rsun

    ! Get total luminosity and star temperature
    Tstar=(Lstar/(4d0*pi*sigmab*Rstar**2d0))**0.25d0

  else
    Mstar=0d0
    rdisk_in=0d0
    Lstar=0d0
    Tstar=0d0
  endif
  if(override .and. Lstar_over/=0d0) then
    Lstar=Lstar_over*lsun
    Tstar=(Lstar/(4d0*pi*sigmab*Rstar**2d0))**0.25d0
  end if
  if(override .and. Tstar_over/=0d0) Tstar=Tstar_over

  if(time<0) print*,""

end subroutine


! Get radius by dichotomy
subroutine get_radius(r,mdot_star)
  use variables,only:rsun
  implicit none
  double precision :: r,mdot_star
  double precision :: rmin,rmax,rcen,fmin,fmax,fcen
  double precision :: prec=1d-3
  double precision :: funcofrad

  rmin=1d-2*rsun
  rmax=1d2*rsun
  rcen=r

  do while(rmax-rmin>prec*rsun)
    fmin=funcofrad(rmin,mdot_star)
    fmax=funcofrad(rmax,mdot_star)
    fcen=funcofrad(rcen,mdot_star)
    if(fcen*fmin>0d0) then
      rmin=rcen
    else
      rmax=rcen
    end if
    rcen=0.5d0*(rmin+rmax)
  end do
  r=rcen
end subroutine



! Three functions to get radius, mdot and mass as a function of each other
double precision function radfrommassmdot(m,mdot_star)
  use variables,only:msun,rsun,min_mstar,max_mstar,HO09_track_resampled,nmass_HO09,HO09_mdot
  implicit none
  double precision, intent(in) :: m, mdot_star
  integer :: i, imdot, imass
  double precision :: imass_r
  double precision :: r1,r2

  ! Determine which mdot will be used to interpolate
  imdot=1
  if (mdot_star > HO09_mdot(1)) then
    do i=1,4
      if (mdot_star > HO09_mdot(i)) then
        imdot=i
      end if
    end do
  end if

  ! Get mass index
  imass_r = 1+(nmass_HO09-1)*dlog(m/min_mstar)/dlog(max_mstar/min_mstar)
  imass_r = min(imass_r,nmass_HO09-1d0)
  imass_r = max(imass_r,1d0)
  imass = floor(imass_r)

  ! Interpolate
  r1 = HO09_track_resampled(imdot,imass,1)*(imass+1d0-imass_r)+HO09_track_resampled(imdot,imass+1,1)*(imass_r-imass)
  r2 = HO09_track_resampled(imdot+1,imass,1)*(imass+1d0-imass_r)+HO09_track_resampled(imdot+1,imass+1,1)*(imass_r-imass)

  radfrommassmdot = ( r1*(dlog10(HO09_mdot(imdot+1))-dlog10(mdot_star)) + r2*(dlog10(mdot_star)-dlog10(HO09_mdot(imdot))) )/(dlog10(HO09_mdot(imdot+1))-dlog10(HO09_mdot(imdot)))

end function


double precision function massfromrad(r)
  use variables, only:rdisk,eta_disk,mcen
  implicit none
  double precision, intent(in) ::r
  double precision :: calc_md,uu,md
  uu=r/rdisk
  md=calc_md(uu)
  massfromrad=(1d0-(1d0-eta_disk)*md)*mcen
end function

! Md factor needed for mass repartition
double precision function calc_md(uu)
  implicit none
  integer :: i
  integer :: n=10000
  double precision, intent(in) :: uu
  double precision :: u,du
  calc_md=0d0
  du=(1d0-uu)/dble(n)
  u=uu+du/2d0
  do i=1,n
    calc_md=calc_md+(1d0-u)**0.5d0*u**(-4d0/3d0)*du
    u=u+du
  end do
  calc_md=calc_md*1d0/3d0*uu**(1d0/3d0)
end function

! Function to get =0
double precision function funcofrad(r,mdot)
  implicit none
  double precision,intent(in) :: r,mdot
  double precision :: radfrommassmdot, massfromrad
  double precision :: m
  m=massfromrad(r)
  funcofrad=radfrommassmdot(m,mdot)-r
end function


! Luminosity from HO09
double precision function lumfrommassmdot(m,mdot_star)
  use variables,only:msun,rsun,min_mstar,max_mstar,HO09_track_resampled,nmass_HO09,HO09_mdot
  implicit none
  double precision, intent(in) :: m, mdot_star
  integer :: i, imdot, imass
  double precision :: imass_r
  double precision :: l1,l2

  ! Determine which mdot will be used to interpolate
  imdot=1
  if (mdot_star > HO09_mdot(1)) then
    do i=1,4
      if (mdot_star > HO09_mdot(i)) then
        imdot=i
      end if
    end do
  end if

  ! Get mass index
  imass_r = 1+(nmass_HO09-1)*dlog(m/min_mstar)/dlog(max_mstar/min_mstar)
  imass_r = min(imass_r,nmass_HO09-1d0)
  imass_r = max(imass_r,1d0)
  imass = floor(imass_r)

  ! Interpolate
  l1 = HO09_track_resampled(imdot,imass,3)*(imass+1d0-imass_r)+HO09_track_resampled(imdot,imass+1,3)*(imass_r-imass)
  l2 = HO09_track_resampled(imdot+1,imass,3)*(imass+1d0-imass_r)+HO09_track_resampled(imdot+1,imass+1,3)*(imass_r-imass)
  lumfrommassmdot = ( l1*(dlog10(HO09_mdot(imdot+1))-dlog10(mdot_star)) + l2*(dlog10(mdot_star)-dlog10(HO09_mdot(imdot))) )/(dlog10(HO09_mdot(imdot+1))-dlog10(HO09_mdot(imdot)))

end function

