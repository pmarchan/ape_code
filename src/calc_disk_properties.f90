subroutine calc_disk_properties(time,xcoord,zcoord)
  use variables
  implicit none
  integer :: i,istart,iend
  double precision, intent(in) :: time,xcoord,zcoord
  double precision :: sigma,temp,r

  r=dsqrt(xcoord*xcoord+zcoord*zcoord)

  ! Global disk properties
  if(disk) then
    rdisk=28.d0*(eta_D/1d19)**(2d0/9d0)*(mcen/2d32)**(1d0/3d0)*masstoflux**(4d0/9d0)*au
  end if
  if(override .and. rdisk_over/=0d0) rdisk=rdisk_over*au

  ! Get star properties (Mass, Radius, Luminosity, Temperature)
  call calc_star_properties(time)

  ! Deduct disk properties from star properties
  Mdisk=Mcen-Mstar
  rdisk_in=dsqrt(Lstar/(4d0*pi*sigmab*Tevap**4d0))
  if(override .and. mdisk_over/=0d0) mdisk=mdisk_over*msun
  if(override .and. rdisk_in_over/=0d0) rdisk_in=rdisk_in_over*au
  sigma0=Mdisk/(2d0*pi*rdisk**2d0*(dexp(-rdisk_in/rdisk)-dexp(-1d0)))

  if (.not. particle .or. (particle .and. r<rmaxdisk .and.  zcoord/xcoord < AspectRatioMax)) then  ! Avoid calculating the whole profile in particle mode

    istart=1
    iend=nprofdisk
    if(particle) then
      istart=floor(r/rmaxdisk*nprofdisk+0.5)
      iend=istart
    end if

    ! Temperature profile
    do i=istart,iend
      if(raddisk(i)<rdisk_in) cycle
      call calc_disk_point(i,sigma,temp)
      sigmadisk(i)=sigma
      tempdisk(i)=temp
      csdisk(i)=dsqrt(kb*temp/(mean*mp))
      Hdisk(i)=csdisk(i)/dsqrt(gg*Mcen/raddisk(i)**3d0)
      densdisk(i)=sigmadisk(i)/(Hdisk(i)*dsqrt(2d0*pi))
    end do

  end if

end subroutine


! Routine to solve the non-linear equation of the disk temperature
subroutine calc_disk_point(i,sigma,temp)
  use variables
  implicit none
  integer,intent(in) :: i
  double precision :: sigma,temp
  double precision :: Y1,Y2,Y3,Y4
  double precision :: omega
  double precision :: dfact=1d-4
  double precision :: dfdt,func,prec,dtemp
  double precision :: eqtemp
  double precision :: prec_obj=1d-6
  integer :: niter
  integer :: niter_max=100
  integer :: reloop
  double precision, save :: temp_calc=50d0

    ! Coefficients calculation
    sigma=sigma0*(raddisk(i)/rdisk)**(-1d0)*dexp(-raddisk(i)/rdisk)
    omega=dsqrt(gg*Mcen/raddisk(i)**3d0)
    Y1=3d0/64d0*kb/(mean*mp)*alphadisk*sigma**2d0*omega
    Y2=5d0/96d0*kb/(mean*mp)*alphadisk*omega
    Y3=sqrt(kb/(mean*mp))/(7d0*raddisk(i)*omega)*sigmab*Tstar**4d0*(Rstar/raddisk(i))**2d0
    Y4=2d0/(3d0*pi)*sigmab*Tstar**4d0*(Rstar/raddisk(i))**3d0+sigmab*temp_mol_cloud**4d0

    temp=temp_calc
    reloop=0

    ! Main iteration loop
    niter=0
    func=eqtemp(temp,raddisk(i),y1,y2,y3,y4)
    prec=abs(func)
    do while(niter<niter_max .and. prec>prec_obj)
      niter=niter+1
      dfdt=(eqtemp(temp*(1d0+dfact),raddisk(i),y1,y2,y3,y4)-eqtemp(temp*(1d0-dfact),raddisk(i),y1,y2,y3,y4))/(temp*2d0*dfact)
      dtemp=func/dfdt
      temp=temp-dtemp
      if(temp<temp_mol_cloud) then
        if(reloop==0) then
          temp=500d0
          reloop=1
        elseif(reloop==1) then ! Safeties in case of non-convergence
          temp=1700d0
          reloop=2
        elseif(reloop==2) then
          temp=1700d0
          exit
        end if
        cycle
      else
        func=eqtemp(temp,raddisk(i),y1,y2,y3,y4)
        prec=abs(func)
      end if
      if(reloop==1 .and. niter==niter_max-1) then
        temp=1700d0
        reloop=2
        niter=1
      endif
    end do
    temp_calc=temp

end subroutine

! Temperature equation
double precision function eqtemp(temp,rad,y1,y2,y3,y4)
  use variables
  implicit none
  double precision,intent(in) :: temp, y1,y2,y3,y4,rad
  double precision :: y1h,y2h
  double precision :: get_kappa,kappa
  double precision :: cs_k,H_k,dens_k,sigma_k
  double precision :: f_C,f_Si,f_Al,fact
 
  sigma_k=sigma0*(rad/rdisk)**(-1d0)*dexp(-rad/rdisk)
  cs_k=dsqrt(kb*temp/(mean*mp))
  H_k=cs_k/dsqrt(gg*Mcen/rad**3d0)
  dens_k=sigma_k/(H_k*dsqrt(2d0*pi))
  kappa=get_kappa(dens_k,temp)

  f_C=max(min((temp-TC2)/(TC1-TC2),1d0),0d0)
  f_Si=max(min((temp-TSi2)/(TSi1-TSi2),1d0),0d0)
  f_Al=max(min((temp-TAl2)/(TAl1-TAl2),1d0),0d0)
  fact=max(C_fraction*f_C+Si_fraction*f_Si+Al_fraction*f_Al,1d-20)
  kappa=fact*kappa

  if(temp>=TAl2) then
    y1h=0d0
    y2h=0d0
  else
    y1h=y1
    y2h=y2
  end if
  eqtemp=sigmab*temp**4d0-y1h*kappa*temp -y2h/kappa*temp-y3*temp**0.5d0-y4
end function



double precision function get_kappa(dens_k,temp)
  ! Density -> Grain size-distribution -> Opacity
  use variables
  implicit none
  double precision, intent(in) :: dens_k,temp
  double precision :: iT,iopac_r,get_grain_index_real
  integer :: iTm,iTp,iopac,iopacp1
  double precision :: kappa1,kappa2

  iopac_r=get_grain_index_real(dens_k,temp)
  iopac=floor(iopac_r)
  iopacp1=min(iopac+1,ngrainpoints)

  if (temp >= tmax_opac) then
    get_kappa=opac_table(iopac,ntemp_opac)
  else
    iT=(temp-tmin_opac)/(tmax_opac-tmin_opac)*(ntemp_opac-1)+1
    iT=min(iT,ntemp_opac+0d0)
    iTm=floor(iT)
    iTp=min(iTm+1,ntemp_opac)
    kappa1=opac_table(iopac,iTm)*(iTp-iT)+opac_table(iopac,iTp)*(iT-iTm)
    kappa2=opac_table(iopacp1,iTm)*(iTp-iT)+opac_table(iopacp1,iTp)*(iT-iTm)
    get_kappa=kappa1*(iopac+1d0-iopac_r)+kappa2*(iopac_r-iopac)
  endif
end function
