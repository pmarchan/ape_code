subroutine write_data(t)
  use variables
  implicit none
  double precision :: t
  integer :: i,j,k
  character(25) ::dataform
  character(4) :: numchar1,numchar2
  character(2) :: kchar
  character(20) :: dirname
  character(5) :: numpartchar
  integer :: nwave=100
  double precision :: xgrain


  ! Write data in radmc-3d ready format
   if (radmc_output) then
     open(101,file="amr_grid.inp",status="replace")
     write(101,"(i1)") 1
     write(101,"(i1)") 0
     write(101,"(i3)") 100
     write(101,"(i1)") 0
     write(101,"(2(i1,2X),i1)") 1,1,0
     write(101,"(2(i4,2X),i4)") nrad,ntheta,1
     write(numchar1,"(i4)") nrad
     write(numchar2,"(i4)") ntheta
     dataform="("//trim(adjustl(numchar1))//"(e15.7,2X),e15.7)"
     write(101,dataform) rad_min_au/1d1*au, rtcell(1:ncell-ntheta+1:ntheta,1)+0.5d0*sizecell(1:ncell-ntheta+1:ntheta)
     dataform="("//trim(adjustl(numchar2))//"(e15.7,2X),e15.7)"
     write(101,dataform) 0d0,0.5d0*(rtcell(1:ntheta-1,2)+rtcell(2:ntheta,2)),pi/2d0
     write(101,dataform) rtcell(1:ntheta,2)*0d0,0d0
     close(101)

     open(102,file="dust_density.inp",status="replace")
     open(202,file="debug.inp",status="replace")
     open(110,file="gas_density.inp",status="replace")
     open(105,file="gas_velocity.inp",status="replace")
     write(202,"(i1)") 1
     write(202,"(i1)") 1
     write(202,"(i1)") 1
     write(102,"(i1)") 1
     write(102,"(i7)") nrad*ntheta
     write(102,"(i2)") nsize
     write(105,"(i1)") 1
     write(105,"(i7)") nrad*ntheta
     ! Dust bins
     do k=1,nsize
       do j=1,ntheta
       do i=1,nrad
         xgrain=grain_dens(igraincell(j+(i-1)*ntheta),k)
         write(102,"(e15.7)") rhocell(j+(i-1)*ntheta)*xgrain
         if(k==1) write(110,"(e15.7)") rhocell(j+(i-1)*ntheta)
         if(k==1) write(105,"(3(e15.7,2X))") vel(j+(i-1)*ntheta,1:3)
       end do
       end do
     end do
     close(102)
     close(105)
     close(110)

     ! Grain sizes
     open(106,file="dust_sizes.inp",status="replace")
     do k=1,nsize
       if (shark_dist) then
         write(106,*) grain_size(k)
       else
         write(106,*) grain_size_chi(1,k)
       end if
     end do
     close(106)

     ! For radiative transfer
     open(103,file="stars.inp",status="replace")
     open(104,file="wavelength_micron.inp",status="replace")
     write(103,"(i1)") 2
     write(103,"(i1,2x,i3)") 1,nwave
     write(103,"(4(e15.7,2x),e15.7)") Rstar,Mstar,0d0,0d0,0d0
     write(104,"(i3)") nwave
     do i=1,nwave
       write(103,"(f8.2)") 0.1*(1d4/0.1d0)**(dble(i-1)/dble(nwave-1))
       write(104,"(f8.2)") 0.1*(1d4/0.1d0)**(dble(i-1)/dble(nwave-1))
     end do
     write(103,"(f6.0)") -Tstar
     close(103)

     open(108,file="dustopac.inp",status="replace")
     write(108,"(I1)") 2
     write(108,"(I2)") nsize
     do k=1,nsize
       write(108,*) "================================================"
       write(108,"(I1)") 1
       write(108,"(I1)") 0
       write(kchar,"(I2)") k-1
       if (shark_dist) then
         write(108,*) "bin"//trim(adjustl(kchar))//"_Leb23"
       else
         write(108,*) "bin"//trim(adjustl(kchar))//"_Mar23"
       end if
     end do


   endif

   ! APE-specific output
   open(200,file="output_ape_sph.dat",status="replace")
   if(loggrid) then
     write(200,*) 1
   else
     write(200,*) 0
   endif
   write(200,"(f10.7,X,A22)") rad_min_au,"-- Minimum radius (au)"
   write(200,"(f8.1,X,A22)") rad_max_au,"-- Maximum radius (au)"
   write(200,"(I5,X,A8)") nrad,"-- nrad"
   write(200,"(f10.7,X,A29)") theta_min,"-- Minimum angle (radians)"
   write(200,"(f10.7,X,A29)") theta_max,"-- Maximum angle (radians)"
   write(200,"(I4,X,A9)") ntheta,"-- ntheta"
   if (t>tff) then
     write(200,"(f8.1,X,A25)") (t-tff)/s2year,"-- Protostar age in years"
   else
     write(200,*)
   endif
   write(200,*) " r (au)           Theta (rad)      Resol (au)       Density (g/cm3)  Temperature (K)  v_r (cm/s)       v_theta (cm/s)   v_phi (cm/s)     v_x (cm/s)       v_y (cm/s)       Av (mag)     Grain index    Location "
   write(200,*) ""
   do i=1,ncell
     write(200,"(11(e15.7,2X),I3,2X,I1)") rtcell(i,1)/au, rtcell(i,2), sizecell(i)/au,rhocell(i),tcell(i),vel(i,1:5),Avcell(i),igraincell(i),wherecell(i)
   end do
   close(200)


   ! Write snapshot grid of part
   if (snapshot_nautilus) then
     call execute_command_line("mkdir particles")
     do i=1,ncell
       write(numpartchar,"(I5)") i
       if(i<10) then
         numpartchar="0000"//trim(adjustl(numpartchar))
       elseif(i<100) then
         numpartchar="000"//trim(adjustl(numpartchar))
       elseif(i<1000) then
         numpartchar="00"//trim(adjustl(numpartchar))
       elseif(i<10000) then
         numpartchar="0"//trim(adjustl(numpartchar))
       end if
       dirname="particles/part_"//trim(adjustl(numpartchar))
       call execute_command_line("mkdir "//dirname)
       open(102,file=trim(adjustl(dirname))//"/structure_evolution.dat",status="replace")
       write(102,*) "! time    log(Av)     log(nH)      log(T)"
       write(102,*) "! (yr)    log(mag)    log(cm-3)   log(K)"
       write(102,format_nautilus) 0d0,                                   log10(Avcell(i)), log10(rhocell(i)*2/(mean*mp)), log10(tcell(i))
       write(102,format_nautilus) nautilus_time_kyr*1d3, log10(Avcell(i)), log10(rhocell(i)*2/(mean*mp)), log10(tcell(i))
       close(102)
     end do
   end if




end subroutine
