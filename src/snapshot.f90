! Snapshot at a given time with full grid
subroutine snapshot(time)
  use variables
  implicit none
  integer :: i,loccell
  double precision, intent(in) :: time
  double precision :: xcoord,zcoord,rho,vr,vphi,vtheta,theta,temp,Av
  double precision :: accreted_mass
  integer :: get_grain_index
  
   ! Information printed on screen
    print*, ""
    print*, "With your parameters :"
    print*, "Star formation time at t=",tff/s2year,"years"
    print*, "Envelope dissipates at t=",2d0*tff/s2year,"years"
    print*, ""

    if (switchresol) then
      print*, ""
      print*, "***************************************************************"
      print*, "Resolution too high, 'resol' increased to",rad_min_au,"au."
      print*, "You can also increase nsampx"
      print*, "***************************************************************"
      print*, ""
    end if

    if(star) then
      print*, "Star formed !"
    else
      print*, "Star not formed before", (tff-time)/s2year, "years"
    end if
    Mcen=accreted_mass(time)


    ! Calculate disk and star properties
    Tstar=0d0
    if(disk) call calc_disk_properties(time,1d0,1d0)
    call compute_envelope_density(time)

    ! Main loop to get cell properties
    do i=1,ncell
      xcoord=xcell(i,1)
      zcoord=xcell(i,2)
      call calc_flow(time,xcoord,zcoord,rho,temp,vr,vphi,vtheta,loccell,Av)

      vel(i,1)=vr
      vel(i,2)=vtheta
      vel(i,3)=vphi
      theta=datan(xcoord/zcoord)
      vel(i,4)=vel(i,1)*dsin(theta)+vel(i,2)*dcos(theta)
      vel(i,5)=vel(i,1)*dcos(theta)-vel(i,2)*dsin(theta)
      rhocell(i)=max(rho*dexp(-(dens_cutoff*au/rtcell(i,1))**2.0),1d-20)
      tcell(i)=temp
      wherecell(i)=loccell
      ! If in outflow, use the grain size-distrib at rho=1e-15
      if (coagulation) then
        if (loccell==4) then
          igraincell(i)=get_grain_index(1d-15,temp)
        elseif (loccell==3) then
          igraincell(i)=ngrainpoints
        else
          igraincell(i)=get_grain_index(rhocell(i),temp)
        end if
      else
        igraincell(i)=1
      endif
      Avcell(i)=Av

    end do

    ! Other informations on screen
    if(star) then
      print*, ""
      print*, "STAR"
      print*, "Star age=", (time-tff)/s2year, "Years"
      print*, "Mstar=", Mstar/msun, "Msun"
      print*, "Rstar=", Rstar/rsun, "Rsun"
      print*, "Lstar=", Lstar/lsun, "Lsun"
      print*, "Tstar=", Tstar, "K"
      print*, ""
    endif
    if(disk) then
      print*, "DISK"
      print*, "Mdisk=", Mdisk/msun, "Msun"
      print*, "Rdisk_reference=", Rdisk/au, "au"
      print*, "Rdisk_outer=", routdisk/au, "au"
      print*,""
    end if

    print*,""
    print*,""
    print*,"*************************************************"
    print*,"WHAT TO DO NEXT ?"
    print*,""
    print*,"** If you want to plot the density map **"
    print*,"$ python3 scripts/plot_map.py"
    print*,"You can change the variable to temperature, velocity, or extinction."
    print*,""
    print*,"** If you want to run radmc3d on this map **"
    if((theta_min_deg>0 .or. theta_max_deg<90) .and. radmc_output) then
      print*, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      print*, "WARNING ! We HEAVILY RECOMMEND that you use theta_min_deg=0d0 and theta_max_deg=90d0 for radmc3d calculations"
      print*, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    endif
    if( .not. radmc_output) print*,"0. Set radmc_output=.true. in parameters.nml, and run ape again"
    print*,"1. Copy lib/*.inp in the current directory"
    print*,"2. $ radmc3d mctherm setthreads N"
    print*,"   With N the number of openMP cpus"
    print*,"3. To plot the temperature: $ python3 scripts/average_temp_radmc3d.py"


    call write_data(time)


end subroutine
