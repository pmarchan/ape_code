const nwave=210
const Tmin=10.0
const Tmax=2000.0
const kb=1.38e-16
const cc=29979245800.0
const hh=6.63e-27
const mp=1.67e-24
const rhogr=2.3
const nrho=128
const nbins=10


function read_kappa(ifile)
  f=open("dustkappa_bin$ifile.inp","r")
  data=readlines(f)
  close(f)
  kappas=Array{Float64,2}(undef,nwave,4)
  for (iline,line) in enumerate(data[3:end])
    values=[x[1] for x in collect(eachmatch(r"(\d\.\d+e[\+\-]\d+)",line))]
    kappas[iline,1:4]=parse.(Float64,values[1:4])
  end
  return kappas
end

function read_all_kappa()
  all_kappas=Any[]
  for i=0:nbins-1
    push!(all_kappas,read_kappa(i))
  end
  return all_kappas
end

function read_rho()
  all_dust=Array{Float64,3}(undef,nrho,nbins,4)

  f2=open("dust_sizes.inp","r")
  sizes=parse.(Float64,readlines(f2))
  close(f2)
  surf=[pi*s^2.0 for s in sizes]
  mass=[4.0/3.0*pi*s^3.0*rhogr for s in sizes]
  for i in 1:nbins
    all_dust[1:end,i,1].=sizes[i]
    all_dust[1:end,i,2].=surf[i]
    all_dust[1:end,i,3].=mass[i]
  end

  f=open("shark_distrib_trunc_10bins.dat","r")
  data=readlines(f)
  close(f)
  rhos=[]
  for (irho,line) in enumerate(data)
    values=[parse(Float64,x[1]) for x in collect(eachmatch(r"(\d+\.\d*[Ee]*[\+\-]*\d*)",line))]
    push!(rhos,values[1])
    for ibin=1:nbins
      all_dust[irho,ibin,4]=values[ibin]/mass[ibin]/(values[1]/(2.3*mp))
    end
  end
  return (rhos,all_dust)
end

all_kappas=read_all_kappa()
(rhos,all_dust)=read_rho()


function dbnudt(nu,T)
  return 2.0*(hh*nu)^2.0*(nu/cc)^2.0/(kb*T^2.0)/(exp(hh*nu/(kb*T))-2.0+exp(-hh*nu/(kb*T)))
  #return 2.0*pi*(nu/cc)^2.0*nu * hh*nu/(kb*T^2)/(exp(hh*nu/(kb*T))-2.0+exp(-hh*nu/(kb*T)))
  #return 2.0*pi*(nu/cc)^2.0*nu * hh*nu/(kb*T^2)*exp(hh*nu/(kb*T))/(exp(hh*nu/(kb*T))-1.0)^2
end

function weighted_kappa(irho,iwave)
  sum_num=0.0
  sum_den=0.0
  for ibin=1:nbins
    imin=max(ibin-1,1)
    imax=min(ibin+1,nbins)
    da=(all_dust[irho,imax,1]-all_dust[irho,imin,1])/(imax-imin)
    pond=all_dust[irho,ibin,3]*all_dust[irho,ibin,4]*da
    #println(all_dust[irho,ibin,1],"   ",all_dust[irho,ibin,3],"    ",all_dust[irho,ibin,4],"    ",pond,"   ",da)
    #pond=all_dust[irho,ibin,2]*all_dust[irho,ibin,4]
    sum_num+= pond*(all_kappas[ibin][iwave,2]+all_kappas[ibin][iwave,3])
    sum_den+= pond
  end
  #stop
  return sum_num/sum_den
end


function calc_ross(T,irho)
  sum_num=0.0
  sum_den=0.0
  for iwave=1:nwave
    wavel=all_kappas[1][iwave,1]
    nu=cc/(wavel/1e4)
    inumin=max(1,iwave-1)
    inumax=min(nwave,iwave+1)
    numin=cc/(all_kappas[1][inumin,1]/1e4)
    numax=cc/(all_kappas[1][inumax,1]/1e4)
    dnu=abs((numax-numin)/(inumax-inumin))
    dbnu=dbnudt(nu,T)
    kappa=weighted_kappa(irho,iwave)
    sum_num+=dbnu*dnu
    sum_den+=dbnu*dnu/kappa
  end
  return sum_num/sum_den
end


Trange=range(Tmin,Tmax,996)
fileout=open("tab_kappas.dat","w")
for (irho,rho) in enumerate(rhos)
  println(irho," /  ",128)
  for (iT,T) in enumerate(Trange)
    kappa=calc_ross(T,irho)
    write(fileout,"$rho   $T   $kappa\n")
  end
end
close(fileout)





